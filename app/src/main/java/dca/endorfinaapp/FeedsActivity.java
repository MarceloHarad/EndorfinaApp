package dca.endorfinaapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class FeedsActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<Media> listMedias;//LINE FOR MEDIAS
    private FirebaseDatabase firebaseDatabase;//FIREBASE DATABASE
    private DatabaseReference databaseReference;//DATABASE REFERENCE
    private ListView photosListView;//LISTVIEW OF PHOTOS
    private FeedAdapter adapterListView;//ADAPTER TO PHOTOS LISTVIEW
    private Context context;
    private Feed feed;
    private Profile profile;//PROFILE OBJECT WITH ALL ATRIBUTES USED
    private Bundle b;//BUNDLE TO RECEIVE AN OBJECT FROM OTHER ACTIVITY
    private Intent intent;//USED TO PASS FROM ACTIVITIES
    private String nomeIntent;//GET USERNAME FROM PROFILE THAT WAS USED ON ANOTHER ACTIVITY
    private ListView listSearchProfile;//SEARCH VIEW TO SEARCH PROFILE
    private SearchView searchProfile;//LISTVIEW OF RESULTS OF SEARCH PROFILE
    private TextView emptyListMessage;//EMPTY LIST MESSAGE
    private Feed feedaux;//AUX OBJECT FEED TO SAVE DATA TO FEED FIREBASE

    //DECLARING BUTTONS
    private ImageButton profileBtn;
    private ImageButton feeds;
    private ImageButton sport;
    private ImageButton photo;
    private ImageButton challenges;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeds);

        intent = getIntent();
        nomeIntent = intent.getStringExtra("name");
        b = intent.getExtras();
        if (b != null) {
            profile = (Profile) getIntent().getSerializableExtra(nomeIntent);
        }

        feed = new Feed();

        context = this;

        firebaseDatabase = firebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("feed");

        //INITIALIZATE FEEDS BUTTON
        feeds = (ImageButton) findViewById(R.id.feeds);
        feeds.setOnClickListener(FeedsActivity.this);

        //INITIALIZATE(WTF?) SPORT BUTTON
        sport = (ImageButton) findViewById(R.id.sports);
        sport.setOnClickListener(FeedsActivity.this);

        //INITIALIZATE(WTF?) CAMERA BUTTON
        photo = (ImageButton) findViewById(R.id.photo);
        photo.setOnClickListener(FeedsActivity.this);

        //INITIALIZATE(WTF?) PROFILE BUTTON
        profileBtn = (ImageButton) findViewById(R.id.profile);
        profileBtn.setOnClickListener(FeedsActivity.this);

        //INITIALIZATE(WTF?) CHALLENGES BUTTON
        challenges = (ImageButton) findViewById(R.id.challenges);
        challenges.setOnClickListener(FeedsActivity.this);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(Feed.class) != null) {
                    feed = dataSnapshot.getValue(Feed.class);
                    feedaux = feed;
                    if (profile.getLastName() != null){
                        for (ListIterator<Media> iter = feed.getListMedias().listIterator(); iter.hasNext(); ) {
                            Media a = iter.next();
                            if (!(profile.getListFans().contains(a.getUserName()))){
                                iter.remove();
                            }
                        }
                    }
                    else{
                        feed.setListMedias(new ArrayList<Media>());
                    }
                } else {
                    feed.setListMedias(new ArrayList<Media>());
                }
                if(!profile.getListFans().isEmpty()){
                    listMedias = feed.getListMedias();
                    photosListView = (ListView) findViewById(R.id.feedListView);
                    adapterListView = new FeedAdapter(context, feed, profile);
                    adapterListView.setFeedaux(feedaux);
                    photosListView.setAdapter(adapterListView);
                    photosListView.setRecyclerListener(new AbsListView.RecyclerListener() {
                        @Override
                        public void onMovedToScrapHeap(View view) {
                        }
                    });
                }
                else{
                    emptyListMessage = (TextView)findViewById(R.id.emptyList);
                    emptyListMessage.setVisibility(View.VISIBLE);
                    photosListView = (ListView) findViewById(R.id.feedListView);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        searchProfile = (SearchView) findViewById(R.id.search_profile);
        listSearchProfile = (ListView) findViewById(R.id.list_search_profile);
        /*
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            SearchProfileAdapter searchProfileAdapter = new SearchProfileAdapter(this);
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchProfileAdapter.doMySearch(query);
            listSearchProfile.setAdapter(searchProfileAdapter);
        }
        */
        final SearchProfileAdapter searchProfileAdapter = new SearchProfileAdapter(context,profile);
        searchProfile.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                query = query.toLowerCase();
                searchProfileAdapter.doMySearch(query);
                listSearchProfile.setAdapter(searchProfileAdapter);
                listSearchProfile.setVisibility(View.VISIBLE);
                photosListView.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.e("FeedsActivity","CHANGE");
                return true;
            }
        });
        searchProfile.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                listSearchProfile.setVisibility(View.INVISIBLE);
                photosListView.setVisibility(View.VISIBLE);
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.feeds){

        }
        if(v.getId() == R.id.profile){
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.photo){
            Intent i = new Intent(getApplicationContext(), UploadMediaActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.sports){
            Intent i = new Intent(getApplicationContext(), FavoriteSportActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.challenges){
            Intent i = new Intent(getApplicationContext(), GameActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
    }

    public void onBackPressed() {
        if(listSearchProfile.getVisibility() == View.VISIBLE){
            listSearchProfile.setVisibility(View.INVISIBLE);
            photosListView.setVisibility(View.VISIBLE);
        }

    }
}

