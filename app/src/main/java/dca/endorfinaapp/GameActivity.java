package dca.endorfinaapp;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageButton feeds;
    private ImageButton sport;
    private ImageButton photo;
    private ImageButton profileBtn;
    private ImageButton challenges;
    private ImageButton notification;
    private Intent intent;
    private String nomeIntent;
    private Bundle b;
    private Profile profile;
    private ArrayList<String> games;
    private FloatingActionButton buttonCreateDuel;
    private ListView listDuels;
    private DuelAdapter duelAdapter;
    private TextView emptyListMessage;

    private FirebaseDatabase firebaseDatabase; // FIREBASE DATABASE
    private FirebaseStorage firebaseStorage; // FIREBASE STORAGE
    private StorageReference storageReference; // REFERENCE TO FIREBASE STORAGE
    private DatabaseReference databaseReference;// REFERENCE TO FIREBASE DATABASE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duel_feeds);

        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = firebaseDatabase.getInstance();

        //INTIALIZATE FIREBASE STORAGE
        firebaseStorage = firebaseStorage.getInstance();

        //INITILIZATE REFERENCE TO STORAGE
        storageReference = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com/");

        //INITIALIZATE REFERENCE TO DATABASE
        databaseReference = firebaseDatabase.getReference();

        //GET PROFILE FROM LAST ACTIVITY
        intent = getIntent();
        nomeIntent = intent.getStringExtra("name");
        b = intent.getExtras();
        if (b != null) {
            profile = (Profile) getIntent().getSerializableExtra(nomeIntent);
        }

        //INITIALIZATE FEEDS BUTTON
        feeds = (ImageButton) findViewById(R.id.feeds);
        feeds.setOnClickListener(GameActivity.this);

        //INITIALIZATE(WTF?) SPORT BUTTON
        sport = (ImageButton) findViewById(R.id.sports);
        sport.setOnClickListener(GameActivity.this);

        //INITIALIZATE(WTF?) CAMERA BUTTON
        photo = (ImageButton) findViewById(R.id.photo);
        photo.setOnClickListener(GameActivity.this);

        //INITIALIZATE(WTF?) PROFILE BUTTON
        profileBtn = (ImageButton) findViewById(R.id.profile);
        profileBtn.setOnClickListener(GameActivity.this);

        //INITIALIZATE(WTF?) CHALLENGES BUTTON
        challenges = (ImageButton) findViewById(R.id.challenges);
        challenges.setOnClickListener(GameActivity.this);

        //INITIALIZATE(WTF?) CHALLENGES BUTTON
        notification = (ImageButton) findViewById(R.id.notification);
        notification.setOnClickListener(GameActivity.this);


        DatabaseReference refFeed = firebaseDatabase.getReference("feedDuel");
        refFeed.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FeedDuel feedDuel = dataSnapshot.getValue(FeedDuel.class);
                int size = feedDuel.getListDuels().size();

                listDuels = (ListView) findViewById(R.id.feedListView);
                if(size != 0){
                    duelAdapter = new DuelAdapter(getApplicationContext(),profile, size, feedDuel.getListDuels());
                    listDuels.setAdapter(duelAdapter);
                }
                else{
                    emptyListMessage = (TextView)findViewById(R.id.emptyList);
                    emptyListMessage.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        buttonCreateDuel = (FloatingActionButton)findViewById(R.id.createDuel);
        buttonCreateDuel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CreateDuel.class);
                i.putExtra("name",profile.getUserName());
                i.putExtra(profile.getUserName(),profile);
                startActivity(i);
            }
        });

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.feeds){
            Intent i = new Intent(getApplicationContext(), FeedsActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.profile){
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.photo){
            Intent i = new Intent(getApplicationContext(), UploadMediaActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.sports){
            Intent i = new Intent(getApplicationContext(), FavoriteSportActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.challenges){

        }

        if(v.getId() == R.id.notification){
            Intent i = new Intent(getApplicationContext(), ListMyDuels.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
    }
}
