
package dca.endorfinaapp;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


//CLASS THAT IS USED BY PHOTOS/VIDEOS
public class Media implements Serializable{

    //ATRIBUTES TO THIS CLASS
    private String imagepath;//IMAGEPATH TO PHOTO ON DEVICE
    private String thumbnailPath;//PATH TO THUMBNAIL
    private String profilePicPath;//PATH TO PROFILE PIC
    private String date;//DATE WHEN MEDIA WAS UPLOADED
    private ArrayList<Comment> comments;//LIST OF COMMENTS IN THE MEDIA
    private List<String> listShares;//LIST OF PEOPLE THAT SHARED THIS PHOTO
    private ArrayList<String> listLikes;//LIST OF LIKES OF THIS MEDIA
    private String description;//DESCRIPTION OF THIS MEDIA
    private String profileFullName;//FULLNAME OF PROFILE THAT UPLOADED MEDIA
    private String profilePath;//PATH OF PROFILE THAT UPLOADED MEDIA
    private String userName;//USERNAME
    private String sport;//MEDIA SPORT


    //GETTERS AND SETTER -------
    public String getImagepath() {
        return imagepath;
    }

    public String getDescription() {
        return description;
    }


    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfileFullName() {
        return profileFullName;
    }

    public void setProfileFullName(String profileFullName) {
        this.profileFullName = profileFullName;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfilePicPath() {
        return profilePicPath;
    }

    public void setProfilePicPath(String profilePicPath) {
        this.profilePicPath = profilePicPath;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public ArrayList<String> getListLikes() {
        return listLikes;
    }

    public void setListLikes(ArrayList<String> listLikes) {
        this.listLikes = listLikes;
    }

    public List<String> getListShares() {
        return listShares;
    }

    public void setListShares(List<String> listShares) {
        this.listShares = listShares;
    }
    //END OF GETTERS AND SETTERS ---------


    //CONSTRUCTOR
    Media() {
        this.description = new String();
        this.imagepath = new String();
        this.thumbnailPath = new String();
        this.profileFullName = new String();
        this.profilePath = new String();
        this.comments = new ArrayList<>();//LIST OF COMMENTS START AS AN EMPTY ARRAYLIST
        this.listLikes = new ArrayList<>();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Media)) {
            return false;
        }

        Media that = (Media) other;

        // Custom equality check here.
        return this.imagepath.equals(that.imagepath)
                && this.thumbnailPath.equals(that.thumbnailPath)
                && this.profilePicPath.equals(that.profilePicPath)
                && this.date.equals(that.date)
                && this.description.equals(that.description)
                && this.profileFullName.equals(that.profileFullName)
                && this.profilePath.equals(that.profilePath)
                && this.userName.equals(that.userName)
                && this.sport.equals(that.sport);
    }

    @Override
    public int hashCode() {
        int hashCode = 1;

        hashCode = hashCode * 37 + this.imagepath.hashCode();
        hashCode = hashCode * 37 + this.thumbnailPath.hashCode();
        hashCode = hashCode * 37 + this.profilePicPath.hashCode();
        hashCode = hashCode * 37 + this.date.hashCode();
        hashCode = hashCode * 37 + this.description.hashCode();
        hashCode = hashCode * 37 + this.profileFullName.hashCode();
        hashCode = hashCode * 37 + this.profilePath.hashCode();
        hashCode = hashCode * 37 + this.userName.hashCode();
        hashCode = hashCode * 37 + this.sport.hashCode();

        return hashCode;
    }

}


