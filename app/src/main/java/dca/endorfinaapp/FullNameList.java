package dca.endorfinaapp;

/**
 * Created by marcelo on 01/06/16.
 */
public class FullNameList {
    private String profilePicPath;//PROFILE PIC PATH
    private String profilePath;//PROFILE PATH
    private String fullName;//FULLNAME PROFILE

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }


    public String getProfilePicPath() {
        return profilePicPath;
    }

    public void setProfilePicPath(String profilePicPath) {
        this.profilePicPath = profilePicPath;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
