package dca.endorfinaapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import static android.support.v4.app.ActivityCompat.startActivityForResult;

//CLASS CREATED TO STORE SOME UTILITY METHODS
public class Utility {

    //METHOD TO CHECK VERSION OF ANDROID TO ASK PERMISSION
    public boolean shouldAskPermission(){

        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);

    }

    //METHOD TO SCALE DOWN A BITMAP IMAGE TO OTHER RESOLUTION
    public Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                            boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }


    public static HashMap<String, Integer> createImageHash(String[] sports, int[] images){
        HashMap<String,Integer> hash = new HashMap<>();
        for(int i = 0; i < sports.length; i++){
            hash.put(sports[i],images[i]);
        }
        return hash;
    }
}
