package dca.endorfinaapp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    //ATRIBUTES DECLARATION
    private EditText loginText; //LOGIN BOX
    private EditText passwordText; //PASSWORD BOX
    private Button loginButton; //LOGIN BUTTON
    private TextView registerLink; //LINK TO START REGISTRATION
    private FirebaseDatabase firebaseDatabase;//FIREBASE DATABASE
    private Profile profile; //PROFILE IMPORTED FROM DATABASE


    //GETTERS AND SETTER----------------
    public EditText getLoginText() {

        return loginText;
    }

    public void setLoginText(EditText loginText) {

        this.loginText = loginText;
    }

    public EditText getPasswordText() {
        return passwordText;
    }

    public void setPasswordText(EditText passwordText) {
        this.passwordText = passwordText;
    }

    public Button getLoginButton() {
        return loginButton;
    }

    public void setLoginButton(Button loginButton) {
        this.loginButton = loginButton;
    }
    //END OF GETTERS AND SETTERS---------------



    //CONSTRUCTOR
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);;

        Drawable background = getResources().getDrawable(R.drawable.background_login);
        getWindow().setBackgroundDrawable(background);


        //FACEBOOK API INITIALIZATION
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        //GET XML
        setContentView(R.layout.activity_login);

        //INITIALIZATE LOGIN BUTTON
        loginButton = (Button) findViewById(R.id.button_login);
        loginButton.setOnClickListener(LoginActivity.this);

        //INITIALIZATE LOGIN TEXT BOX
        loginText = (EditText) findViewById(R.id.email_address);
        passwordText = (EditText) findViewById(R.id.password);

        //INITILIZATE LINK TO REGISTRATION
        registerLink = (TextView) findViewById(R.id.register_link);
        registerLink.setOnClickListener(new View.OnClickListener(){
            //METHOD IF YOU CLICK IN THIS LINK
            public void onClick(View v){
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = firebaseDatabase.getInstance();

        //INITIALZIATE PROFILE AS NULL
        profile = null;

    }

    @Override

    //METHOD IF YOU CLICK LOGIN BUTTON
    public void onClick(View v) {


        //START LOADING DIALOG WHILE PROFILE HAS NOT BEEN DOWNLOADED FROM DATABASE
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while we check your profile...");
        progress.show();


        //IF YOU WANT TO DELETE SQLITE DATABASE, TAKE OFF COMMENT ON NEXT LINE
        //getApplicationContext().deleteDatabase("profileDB.db");

        //GET REFERENCE TO FIREBASE DATABASE FROM USERNAME
        DatabaseReference ref = firebaseDatabase.getReference("profiles/" + loginText.getText().toString());

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        profile = dataSnapshot.getValue(Profile.class);

                        //CLOSE LOADING DIALOG
                        progress.dismiss();

                        checkLogin();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

    }

    private void checkLogin(){

        if (profile != null && Objects.equals(profile.getPassword(), passwordText.getText().toString())){

            //GOTO PROFILE ACTIVITY WITH PROFILE AND USERNAME AS EXTRAS.
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }

        //IF USERNAME OR PASSWORD WEREN'T CORRECT
        else{


                //ALERT DIALOG SAYING ONLY PASSWORD WAS WRONG
                AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
                helpBuilder.setTitle("Error");
                helpBuilder.setMessage("Your user name or your password are not correct.");
                helpBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                AlertDialog helpDialog = helpBuilder.create();
                helpDialog.show();
        }
    }


    //METHOD CHANGING ACTION WHEN BACK BUTTON IS PRESSED
    public void onBackPressed() {

        //ISTEAD OF GOING ONE ACTIVITY BACK, APP IS WITHDRAWED
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }
}
