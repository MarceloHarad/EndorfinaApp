package dca.endorfinaapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.util.ArrayList;


//ACTIVITY THAT SHOWS YOUR PROFILE
public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{

    //ATRIBUTES FROM THIS CLASS
    private Intent intent;//USED TO PASS FROM ACTIVITIES
    private String nomeIntent;//GET USERNAME FROM PROFILE THAT WAS USED ON ANOTHER ACTIVITY
    private TextView nome;//TEXTVIEW THAT SHOWS PROFILE NAME
    private ImageButton photo;//BUTTON TO UPLOAD MEDIA TO YOUR PROFILE
    private Button logout;//BUTTON USED TO LOGOUT FROM PROFILE
    private ImageButton feeds;//BUTTON THAT TAKES YOU TO THE FEEDS PAGE
    private ImageButton challenges;//BUTTON THAT TAKES YOU TO THE GAMES PAGE
    private ImageButton sport;//BUTTON THAT TAKES YOU TO THE SPORT ACTIVITY
    private ImageView profilePic;//CIRCLE IMAGEVIEW THAT SHOWS YOUR PROFILE PICTURE
    private ImageView coverPhoto;//IMAGEVIEW THAT SHOWS YOUR COVER PHOTO
    private final static int SELECT_PHOTO = 12345;//AUXILIAR VALUE TO ACCESS GALLERY
    private Utility utility;//UTILITY OBJECT TO GET SOME UTILITY METHODS
    private GridView gridView;//GRIDVIEW CONTAINING IMAGEVIEWS WITH MEDIAS
    private MyAdapter gridAdapter;//ADAPTER TO PUT IMAGEVIEWS INSIDE THE GRIDVIEW
    private ProgressBar progressHeader;//PROGRESSBAR FROM HEADER
    private ProgressBar progressProfile;//PROGRESSBAR FROM PROFILE
    private Profile profile;//PROFILE OBJECT WITH ALL ATRIBUTES USED
    private Bundle b;//BUNDLE TO RECEIVE AN OBJECT FROM OTHER ACTIVITY
    private String fullName;//FULLNAME OF YOUR PROFILE

    private FirebaseDatabase firebaseDatabase;
    private FirebaseStorage firebaseStorage;

    @Override
    //CONSTRUCTOR
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //GET PROFILE FROM LAST ACTIVITY
        intent = getIntent();
        nomeIntent = intent.getStringExtra("name");
        b = intent.getExtras();
        if (b != null){
            profile = (Profile)getIntent().getSerializableExtra(nomeIntent);
        }

        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = firebaseDatabase.getInstance();

        //INITIALIZATE FIREBASE STORAGE
        firebaseStorage = firebaseStorage.getInstance();
        //INITIALIZATE UPLOAD MEDIA BUTTON
        photo = (ImageButton) findViewById(R.id.photo);
        photo.setOnClickListener(ProfileActivity.this);

        //INITIALIZATE PROGRESS BAR HEADER
        progressHeader = (ProgressBar)findViewById(R.id.loadingHeader);

        //INITIALIZATE PROGRESS BAR PROFILE
        progressProfile = (ProgressBar)findViewById(R.id.loadingProfile) ;

        //INITIALIZATE PROFILE PICTURE AND UPLOAD PICTURE IF IT IS ALREADY ON THE DATABASE
        profilePic = (ImageView)findViewById(R.id.profile_image);
        profilePic.setOnClickListener(ProfileActivity.this);
        if (!profile.getProfilePic_path().toString().isEmpty()){
            progressProfile.setVisibility(View.VISIBLE);
            StorageReference storageRef = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com");
            StorageReference photosRef = storageRef.child(profile.getProfilePic_path());


            final long ONE_MEGABYTE = 1024 * 1024;
            photosRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                    profilePic.setImageBitmap(bitmap);
                    progressProfile.setVisibility(View.GONE);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                }
            });

        }
        else{
            profilePic.setImageDrawable(getResources().getDrawable(R.drawable.defaultprofile));
        }

        //INITIALIZATE COVER PHOTO AND UPLOAD PHOTO IF IT IS ALREAY ON THE DATABASE
        coverPhoto = (ImageView)findViewById(R.id.header);
        coverPhoto.setOnClickListener(ProfileActivity.this);
        if (!profile.getCover_path().toString().isEmpty()){
            progressHeader.setVisibility(View.VISIBLE);
            StorageReference storageRef = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com");
            StorageReference photosRef = storageRef.child(profile.getCover_path());

            final long ONE_MEGABYTE = 1024 * 1024;
            photosRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                    coverPhoto.setImageBitmap(bitmap);
                    progressHeader.setVisibility(View.GONE);
                    coverPhoto.setVisibility(View.VISIBLE);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                }
            });

        }
        else{
        }


        //INITIALIZATE LOGOUT BUTTON
        logout = (Button) findViewById(R.id.logout);
        logout.setOnClickListener(ProfileActivity.this);

        //INITIALIZATE FEEDS BUTTON
        feeds = (ImageButton) findViewById(R.id.feeds);
        feeds.setOnClickListener(ProfileActivity.this);

        //INITIALIZATE(WTF?) SPORT BUTTON
        sport = (ImageButton) findViewById(R.id.sports);
        sport.setOnClickListener(ProfileActivity.this);

        //INITIALIZATE(WTF?) CHALLENGES BUTTON
        challenges = (ImageButton) findViewById(R.id.challenges);
        challenges.setOnClickListener(ProfileActivity.this);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int heightScreen = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        coverPhoto.getLayoutParams().height = width*20/48;


        //INITILIZATE GRIDVIEW ADAPTER WITH GRIDVIEW
        gridAdapter = new MyAdapter(this,heightScreen,width,profile);
        gridView = (GridView)findViewById(R.id.gridView);
        gridView.setAdapter(gridAdapter);


        //INITIALIZATE FULLNAME AND SHOW IT ON TEXTVIEW
        fullName = profile.getFirstName() + " " + profile.getLastName();
        nome = (TextView)findViewById(R.id.fullNameDisplay);
        nome.setText(fullName);


        //INITIALIZATE UTILITY OBJECT
        utility = new Utility();


        //ADD MEDIAS TO GRIDVIEW FROM PROFILE ATRIBUTE(LISTPHOTOS)
        for (Media i:profile.getListPhotos()){
            gridAdapter.addItemtoList(i);
        }

    }


    //ALL METHODS RELATED TO CLICK BUTTONS
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {

        //IF BUTTON PRESSED WAS TO UPLOAD A MEDIA
        if(v.getId() == R.id.photo){


            //GOTO ACTIVITY USED TO UPLOAD MEDIA, PASSING PROFILE TO NEXT ACTIVITY
            Log.e("ProfileActivity","Marshmallow");
            Intent i = new Intent(getApplicationContext(), UploadMediaActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);


        }

        //IF BUTTON PRESSED WAS TO LOGOUT
        if(v.getId() == R.id.logout){

            //CHECK IF PERSON REALLY WANTS TO LOGOUT CREATING ALERT DIALOG WITH YES/NO OPTION. IF YES, GOTO LOGIN ACTIVITY
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int choice) {
                    switch (choice) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.logout)
                    .setPositiveButton(R.string.yes, dialogClickListener)
                    .setNegativeButton(R.string.no, dialogClickListener).show();

        }


        //IF BUTTON PRESSED WAS TO UPLOAD PROFILE IMAGE
        if(v.getId() == R.id.profile_image){

            //ASK IF PERESON REALLY WANTS TO CHANGE PROFILE PICTURE, IF YES, GOTO UPLOAD PROFILE PICTURE ACTIVITY PASSING PROFILE OBJECT.
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int choice) {
                    switch (choice) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent i = new Intent(getApplicationContext(), ChangeProfilePic.class);
                            i.putExtra("name",profile.getUserName());
                            i.putExtra("typePhoto","profile");
                            i.putExtra(profile.getUserName(),profile);
                            startActivity(i);
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you want to change your profile picture?")
                    .setPositiveButton(R.string.yes, dialogClickListener)
                    .setNegativeButton(R.string.no, dialogClickListener).show();
        }

        //SAME AS CHANGE PROFILE PICTURE, BUT WITH COVER PHOTO AND CHANGING TYPEPHOTO TO COVER
        if(v.getId() == R.id.header){
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int choice) {
                    switch (choice) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent i = new Intent(getApplicationContext(), ChangeProfilePic.class);
                            i.putExtra("name",profile.getUserName());
                            i.putExtra("typePhoto","cover");
                            i.putExtra(profile.getUserName(),profile);
                            startActivity(i);
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you want to change your cover photo?")
                    .setPositiveButton(R.string.yes, dialogClickListener)
                    .setNegativeButton(R.string.no, dialogClickListener).show();
        }

        //IF BUTTON PRESSED WAS TO FEEDS PAGE
        if(v.getId() == R.id.feeds){
            Intent i = new Intent(getApplicationContext(), FeedsActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.sports){
            Intent i = new Intent(getApplicationContext(), FavoriteSportActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.challenges){
            Intent i = new Intent(getApplicationContext(), GameActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }


    }



    @Override
    //CHANGE ACTION WHEN BACK BUTTON IS PRESSED. ISTEAD OF GOING TO LAST ACTIVITY, WITHDRAW APP
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }
}
