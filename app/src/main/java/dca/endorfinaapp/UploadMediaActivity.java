package dca.endorfinaapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


//ACTIVITY USED TO UPLOAD MEDIA(PHOTO/VIDEO)
public class UploadMediaActivity extends AppCompatActivity implements View.OnClickListener{


    //ALL ATRIBUTES USED BY THIS CLASS
    FloatingActionButton uploadPhoto;//BUTTON TO UPLOAD A NEW PHOTO
    FloatingActionButton cropButton;//BUTTON TO CROP PHOTO AFTER SELECTION
    Button shareMedia;//BUTTON TO FINISH AND SHARE MEDIA
    ImageView previewPhoto;//IMAGEVIEW TO PREVIEW YOUR MEDIA
    Utility utility;//UTILITY OBJECT TO UTILITY METHODS
    CropImageView cropImageView;//CROPIMAVIEW TO CROP PHOTO
    EditText description;//BOX TO WRITE DESCRIPTION OF MEDIA
    AutoCompleteTextView listSports;//LIST OF SPORTS
    private Bitmap cropped;// BITMAP OF IMAGE AFTER BEING CROPPED
    final int SELECT_PHOTO = 12345;// AUXILIAR VALUE TO SELECT GALLERY

    private Profile profile;//PROFILE OF PERSON THAT WANTS TO ADD MEDIA
    private Intent intent;//ATRIBUTE TO PASS THROUGH ACTIVITIES
    private String nomeIntent;//NAME OF PROFILE TO GET PROFILE FROM ACTIVITY
    private Bundle b;//BUNDLE TO GET OBJECT FROM OTHER ACTIVITY
    private String path;//PATH OF THE IMAGE UPLOADED
    private Bitmap bit;//

    private FirebaseDatabase firebaseDatabase;//FIREBASE DATABASE

    private FirebaseStorage firebaseStorage;//FIREBASE STORAGE

    private StorageReference storageReference;//REFERENCE TO FIREBASE STORAGE

    private Context context;//CONTEXT OF THIS CLASS

    private Feed feed;//FEED CLASS


    @Override

    //CONSTRUCTOR
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_media);


        //GET PROFILE FROM PROFILE ACTIVITY
        intent = getIntent();
        nomeIntent = intent.getStringExtra("name");
        b = intent.getExtras();
        if (b != null){
            profile = (Profile)getIntent().getSerializableExtra(nomeIntent);
        }


        //INITIALIZATE UPLOAD PHOTO BUTTON
        uploadPhoto = (FloatingActionButton) findViewById(R.id.uploadPhoto);
        uploadPhoto.setOnClickListener(UploadMediaActivity.this);


        //INITIALIZATE CROP BUTTON
        cropButton = (FloatingActionButton) findViewById(R.id.cropButton);
        cropButton.setOnClickListener(UploadMediaActivity.this);


        //INITIALIZATE PRREVIEW IMAGEVIEW
        previewPhoto = (ImageView)findViewById(R.id.previewPhoto);

        //INITIALIZATE CROP IMAGEVIEW AND SET VISIBILITY OF VIEW TO INVISIBLE
        cropImageView = (CropImageView)findViewById(R.id.cropImageView);
        cropImageView.setVisibility(cropImageView.INVISIBLE);
        cropImageView.setFixedAspectRatio(true);

        //INITIALIZATE SHARE MEDIA BUTTON
        shareMedia = (Button)findViewById(R.id.buttonShareMedia);
        shareMedia.setOnClickListener(UploadMediaActivity.this);

        //INITIALIZATE DESCRIPTION BOX
        description = (EditText)findViewById(R.id.description);

        //INITIALIZATE UTILITY OBJECT TO UTILITY METHODS
        utility = new Utility();

        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = firebaseDatabase.getInstance();

        //INITIALIZATE FIREBASE STORAGE
        firebaseStorage = firebaseStorage.getInstance();

        //INITIALIZATE STORAGE REFERENCE
        storageReference = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com");

        //INITIALIZATE CONTEXT
        context = this;

        //INITIALIZATE FEED
        feed = new Feed();
        feed.uploadListFeed();


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line,getResources().getStringArray(R.array.new_sport_list));
        listSports = (AutoCompleteTextView) findViewById(R.id.sportsView);
        listSports.setAdapter(adapter);

    }


    //METHODS WHEN YOU CLICK ON ANY BUTTON
    @Override
    public void onClick(View v) {

        //IF YOU CLICK ON UPLOAD PHOTO BUTTON
        if(v.getId() == R.id.uploadPhoto){

            //ASK PERMISSION TO ENTER GALLERY IF ANDROID VERSION IS MARSHMALLOW
            if (utility.shouldAskPermission()){

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            }

            //ENTER GALLERY
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        }

        //IF BOTTOM PRESSED IS CROPPED BUTTON
        if(v.getId() == R.id.cropButton){

            cropped = cropImageView.getCroppedImage();//GET IMAGE FROM CROP VIEW
            previewPhoto.setImageBitmap(cropped);//SET PREVIEW IMAGE VIEW TO CROPPED IMAGE(PROFILE PIC)
            cropImageView.setVisibility(cropImageView.INVISIBLE);//SET VISIBILITY OF CROPVIEW TO INVISIBLE
            previewPhoto.setVisibility(previewPhoto.VISIBLE);//SET VISIBILITY OF PREVIEW PHOTO TO VISIBLE(PROFILE PIC)
            cropButton.setVisibility(cropButton.INVISIBLE);//SET VISIBILITY OF CROP BUTTON TO INVISIBLE
            uploadPhoto.setVisibility(uploadPhoto.VISIBLE);// SET VISIBILITY OF UPLOAD PHOTO TO VISIBLE
        }

        //IF BUTTON PRESSED IS TO SHARE MEDIA
        if(v.getId() == R.id.buttonShareMedia){

            //IF YOU`VE ALREADY UPLOADED A PHOTO
            if(previewPhoto.getDrawable() != null){

                //IF YOU`VE ALREADY WRITTEN A DESCRIPTION
                if(!description.getText().toString().isEmpty()){

                    if(Arrays.asList(getResources().getStringArray(R.array.new_sport_list)).contains(listSports.getText().toString())){
                        //CREATE LOADING DIALOG WHILE PHOTO HAS BEEN UPLOADED
                        final ProgressDialog progress = new ProgressDialog(this);
                        progress.setTitle("Loading");
                        progress.setMessage("Wait while your photo is being uploaded...");
                        progress.show();

                        //storeImage(cropped);//STORE IMAGE ON INTERNAL STORAGE(AND GET PATH FROM IT)
                        final String pathMedia = "media/" + profile.getUserName() + "/" + profile.getListPhotos().size() + ".jpg";
                        final String pathThumbnail = "ThumbnailMedia/" + profile.getUserName() + "/" + profile.getListPhotos().size() + ".jpg";
                        StorageReference refToMedia = storageReference.child(pathMedia);
                        final StorageReference refToThumbnail = storageReference.child(pathThumbnail);

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                        Bitmap croppedThumbNail = utility.scaleDown(cropped,250,true);
                        cropped = utility.scaleDown(cropped,1000,true);
                        cropped.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                        croppedThumbNail.compress(Bitmap.CompressFormat.JPEG, 60, baos2);

                        final byte[] data = baos.toByteArray();
                        final byte[] data2 = baos2.toByteArray();

                        UploadTask uploadTask = refToMedia.putBytes(data);
                        uploadTask.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle unsuccessful uploads
                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                UploadTask uploadTask2 = refToThumbnail.putBytes(data2);
                                uploadTask2.addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        // Handle unsuccessful uploads
                                    }
                                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        //Calendar date = Calendar.getInstance();//GET EXACT TIME OF UPLOAD
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                                        String strDate = sdf.format(c.getTime());
                                        String[] elementsDate = strDate.split("\\s+");
                                        String finalDate = elementsDate[0] + " at " + elementsDate[1];
                                        finalDate = finalDate.replace("-","/");

                                        Media media = new Media();//CREATE NEW MEDIA WITH DESCRIPTION AND PATH OF IMAGE
                                        media.setDescription(description.getText().toString());
                                        media.setImagepath(pathMedia);
                                        media.setThumbnailPath(pathThumbnail);
                                        media.setProfileFullName(profile.getFirstName() + " " + profile.getLastName());
                                        media.setProfilePath("profiles/" + profile.getUserName());
                                        media.setDate(finalDate);
                                        media.setUserName(profile.getUserName());
                                        media.setProfilePicPath(profile.getProfilePic_path());
                                        media.setSport(listSports.getText().toString());

                                        DatabaseReference refFeeds = firebaseDatabase.getReference("feed");
                                        //refFeeds = refFeeds.child(media.getUserName() + "-" + media.getDate());
                                        feed.addMedia(media);
                                        refFeeds.setValue(feed);

                                        profile.getListPhotos().add(media);//ADD MEDIA TO PROFILE LIST OF MEDIAS
                                        DatabaseReference ref = firebaseDatabase.getReference("profiles/" + profile.getUserName());
                                        ref.child("listPhotos").setValue(profile.getListPhotos());

                                        //CLOSE LOADING DIALOG
                                        progress.dismiss();

                                        //CREATE ALERT DIALOG TO SHOW THAT PHOTO WAS SHARED WITH SUCCESS
                                        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(context);
                                        helpBuilder.setTitle("Success");
                                        helpBuilder.setMessage("Your media was successfully published");
                                        helpBuilder.setPositiveButton("Ok",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
                                                        i.putExtra("name",profile.getUserName());
                                                        i.putExtra(profile.getUserName(),profile);
                                                        startActivity(i);
                                                    }
                                                });

                                        AlertDialog helpDialog = helpBuilder.create();
                                        helpDialog.show();
                                    }
                                });
                            }
                        });
                    }
                    else{
                        listSports.setError("This sport is not registered. Choose a valid sport");
                    }
                }
            }
        }
    }

    @Override
    //METHOD CALLED WHEN PHOTO WAS CHOOSED FROM GALLERY
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            Uri pickedImage = data.getData();//IMAGE URI
            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));//IMAGE PATH

            //TRANSFROM IMAGEPATH TO BITMAP
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

            previewPhoto.setVisibility(previewPhoto.INVISIBLE);// SET VISIBILITY OF PREVIEW PHOTO TO INVISIBLE

            cropImageView.setImageBitmap(bitmap);//SET CONTENT OF CROPVIEW TO BITMAP
            cropImageView.setVisibility(cropImageView.VISIBLE);//SET VISIBILITY OF CROPVIEW TO VISIBLE
            uploadPhoto.setVisibility(uploadPhoto.INVISIBLE);//SET VISIBILITY OF UPLOADPHOTO BUTTON TO INVISIBLE
            cropButton.setVisibility(cropButton.VISIBLE);//SET VISIBILITY OF CROP BUTTON TO VISIBLE

            cursor.close();
        }
    }
}
