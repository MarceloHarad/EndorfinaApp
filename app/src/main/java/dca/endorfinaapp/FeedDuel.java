package dca.endorfinaapp;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by marcelo on 13/06/16.
 */
public class FeedDuel {

    private ArrayList<String> listDuels;//LIST OF MEDIAS
    private FirebaseDatabase firebaseDatabase;//FIREBASE DATABASE
    private DatabaseReference databaseReference;//REFERENCE TO DATABASE

    public FeedDuel() {
        listDuels = new ArrayList<>();
        firebaseDatabase = firebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("feedDuel");
    }

    public ArrayList<String> getListDuels() {
        return this.listDuels;
    }

    public void setListDuels(ArrayList<String> listMedias) {
        this.listDuels = listMedias;
    }

    public void addDuel(String duel){
        this.listDuels.add(duel);
    }

    public void uploadListFeed(){


        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(FeedDuel.class) != null){
                    FeedDuel feed = dataSnapshot.getValue(FeedDuel.class);
                    listDuels = feed.getListDuels();
                }
                else{
                    listDuels = new ArrayList<String>();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
