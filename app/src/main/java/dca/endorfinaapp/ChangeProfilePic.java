package dca.endorfinaapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;


//ACITIVTY USED TO CHANGE PROFILE PIC AND COVER PHOTO, REALLY SIMILAR TO UPLOAD A MEDIA
public class ChangeProfilePic extends AppCompatActivity implements View.OnClickListener {


    //ATRIBUTES USED BY THIS CLASS
    ImageButton uploadProfilePhoto;//BUTTON TO UPLOAD A PHOTO
    ImageButton cropProfileButton;//BUTTON TO CROP A PHOTO
    Button finishProfilePhoto;//BUTTON TO FINISH CHANGING A PHOTO
    ImageView previewProfilePhoto;//IMAGEVIEW TO PREVIW PIC(AND COVER)
    CircleImageView previewProfilePhoto2;//CIRCLE IMAGEVIEW TO PREVIEW PROFILE PIC
    Utility utility;// UILITY OBJECT TO CALL UTILITY METHODS
    CropImageView cropImageView;// CROP IMAGE VIEW TO CROP IMAGE
    String typePhoto;// TYPE OF PHOTO (PROFILE PIC OR COVER), PASSED FROM PROFILE ACTIVITY
    private Bitmap cropped;// BITMAP OF IMAGE AFTER CROP
    final int SELECT_PHOTO = 12345;//AUXILIAR VALUE TO ENTER GALLERY

    private Profile profile;//PROFILE OBJECT PASSED FROM PROFILE ACTIVITY
    private Intent intent;//USED TO PASS FROM ONE ACTIVITY TO ANOTHER
    private String nomeIntent;//NAME OF PROFILE TO GET OBJECT FROM ACTIVITY
    private Bundle b;//BUNDLE USED TO GET PROFILE OBJECT FROM OTHER ACTIVITY
    private String path;//PATH OF IMAGE UPLOADED
    private Context context;

    //SQLITE DATABASE

    private FirebaseDatabase firebaseDatabase;//FIREBASE DATABASE

    private FirebaseStorage firebaseStorage;//FIREBASE STORAGE

    @Override
    //CONSTRUCTOR
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_profile_pic);

        //GET PROFILE FROM PROFILE ACTIVITY
        intent = getIntent();
        nomeIntent = intent.getStringExtra("name");
        typePhoto = intent.getStringExtra("typePhoto");
        b = intent.getExtras();
        if (b != null){
            profile = (Profile)getIntent().getSerializableExtra(nomeIntent);
        }

        //INITIALIZATE PREVIEW PROFILE PHOTO VIEW
        previewProfilePhoto = (ImageView)findViewById(R.id.previewProfilePhoto);

        //INITIALIZATE UPLOAD PROFILE PIC BUTTON
        uploadProfilePhoto = (ImageButton)findViewById(R.id.uploadProfilePhoto);
        uploadProfilePhoto.setOnClickListener(ChangeProfilePic.this);

        //INITIALIZATE CROP PIC BUTTON
        cropProfileButton = (ImageButton)findViewById(R.id.cropProfileButton);
        cropProfileButton.setOnClickListener(ChangeProfilePic.this);

        //INITIALIZATE CROP IMAGE VIEW
        cropImageView = (CropImageView)findViewById(R.id.cropImageView);
        cropImageView.setVisibility(cropImageView.INVISIBLE);

        //INITIALIZATE BUTTON TO FINISH PHOTO
        finishProfilePhoto = (Button)findViewById(R.id.buttonFinishProfilePic);
        finishProfilePhoto.setOnClickListener(ChangeProfilePic.this);

        //INITIALIZATE PREVIEW OF PROFILE PIC(CIRCLE)
        previewProfilePhoto2 = (CircleImageView)findViewById(R.id.previewProfilePhoto2);

        //INITIALIZATE UTILITY OBJECT
        utility = new Utility();

        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = firebaseDatabase.getInstance();

        //INITIALIZATE FIREBASE STORAGE
        firebaseStorage = firebaseStorage.getInstance();

        //INITIALIZATE CONTEXT
        context = this;
    }

    @Override
    //METHODS CALLED WHEN A BUTTON IS CLICKED
    public void onClick(View v) {

        //IF YOU CLICK UPLOAD A PHOTO BUTTON
        if(v.getId() == R.id.uploadProfilePhoto){

            //CHECK IF ANDROID VERSION IS MARSHMALLOW
            if (utility.shouldAskPermission()){

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            }
            //ENTER GALLERY TO CHOOSE PICTURE
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        }

        //IF BUTTON CLICKED IS CROP PHOTO
        if(v.getId() == R.id.cropProfileButton){

            cropped = cropImageView.getCroppedImage();//GET CROP IMAGE FROM CROPIMAGEVIEW
            previewProfilePhoto2.setImageBitmap(cropped);//INSERT CROPPED IMAGE IN PROFILE PICTURE VIEW
            previewProfilePhoto.setImageBitmap(cropped);//INSERT CROPPED IMAGE IN COVER PHOTO VIEW
            cropImageView.setVisibility(cropImageView.INVISIBLE);//CHANGE VISIBILITY OF CROPIMAGEVIEW TO INVISIBLE

            //CHECK IF IT'S CHANGING PROFILE PICTURE OR COVER PICTURE
            if(Objects.equals(typePhoto, "profile")){
                previewProfilePhoto2.setVisibility(previewProfilePhoto.VISIBLE);
            }
            if(Objects.equals(typePhoto, "cover")){
                previewProfilePhoto.setVisibility(previewProfilePhoto.VISIBLE);
            }
            //SET VISIBILITY OF CROP BUTTON TO INVISIBLE AND UPLOAD PHOTO TO VISIBLE.
            cropProfileButton.setVisibility(cropProfileButton.INVISIBLE);
            uploadProfilePhoto.setVisibility(uploadProfilePhoto.VISIBLE);
            finishProfilePhoto.setVisibility(View.VISIBLE);
        }

        //IF BUTTON CLICKED IS FINISHED PHOTO
        if(v.getId() == R.id.buttonFinishProfilePic){


            //IF YOU HAVE ACTUALLY CHOOSED A PHOTO AND CROPPED IT
            if(previewProfilePhoto2.getDrawable() != null && previewProfilePhoto.getDrawable() != null){

                //START LOADING WHILE PHOTO HAS NOT BEEN UPLOADED
                final ProgressDialog progress = new ProgressDialog(this);
                progress.setTitle("Loading");
                progress.setMessage("Wait while your photo is being uploaded...");
                progress.show();

                //UPLOAD IMAGE TO FIREBASE DATABASE
                StorageReference storageRef = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com");
                final String firebasePath = typePhoto + "photos/"+ profile.getUserName() + ".jpg";
                StorageReference photosRef = storageRef.child(firebasePath);


                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                cropped = utility.scaleDown(cropped,500,true);
                cropped.compress(Bitmap.CompressFormat.JPEG, 75, baos);
                byte[] data = baos.toByteArray();

                UploadTask uploadTask = photosRef.putBytes(data);

                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Log.e("ChangeProfilePic","UPLOAD FAILED");
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {


                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                        DatabaseReference ref = firebaseDatabase.getReference("profiles/" + profile.getUserName());
                        //CHECK IF IT IS A PROFILE PICTURE OR COVER PICTURE TO ADD TO PROFILE AND DATABASE
                        if(Objects.equals(typePhoto, "profile")){
                            profile.setProfilePic_path(firebasePath);//ADD PHOTO TO PROFILE
                            DatabaseReference refFullName = firebaseDatabase.getReference("listFullName/" +profile.getFirstName().toLowerCase() + " " + profile.getLastName().toLowerCase() + "/profilePicPath");
                            refFullName.setValue(profile.getProfilePic_path());
                            ref.child("profilePic_path").setValue(firebasePath);
                        }
                        if(Objects.equals(typePhoto, "cover")){
                            profile.setCover_path(firebasePath);//ADD COVER TO PROFILE
                            ref.child("cover_path").setValue(firebasePath);
                        }


                        //CLOSE LOADING DIALOG
                        progress.dismiss();

                        //CREATE ALERT DIALIG TO SHOW THAT PROFILE/COVER PHOTO WAS CHANGED
                        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(context);
                        helpBuilder.setTitle("Success");

                        //CHECK IF IT WAS A PROFILE OF COVER PHOTO THAT WAS CHANGED
                        if(Objects.equals(typePhoto, "profile")){
                            helpBuilder.setMessage("Your profile picture was changed");
                        }
                        if(Objects.equals(typePhoto, "cover")){
                            helpBuilder.setMessage("Your cover photo was changed");
                        }

                        helpBuilder.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
                                        i.putExtra("name",profile.getUserName());
                                        i.putExtra(profile.getUserName(),profile);
                                        startActivity(i);
                                    }
                                });

                        AlertDialog helpDialog = helpBuilder.create();
                        helpDialog.show();

                    }
                });

            }
        }
    }


    @Override
    //METHOD CALLED WHEN YOU CHOOSE A PHOTO FROM THE GALLERY
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            Uri pickedImage = data.getData();
            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            //CREATE BITMAP BASED ON IMAGEPATH
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

            previewProfilePhoto.setVisibility(previewProfilePhoto.INVISIBLE);//SET VISIBILITY OF PREVIEW PROFILE PICTURE TO INVISIBLE
            //CHECK IF IT IS A PROFILE PICTURE OF COVER PICTURE TO CHANGE SHAPE OF CROP IMAGEVIEW
            if(Objects.equals(typePhoto, "profile")){
                cropImageView.setCropShape(CropImageView.CropShape.OVAL);//CHANGE SHAPE OF CROP VIEW TO OVAL
                cropImageView.setFixedAspectRatio(true);
            }
            if(Objects.equals(typePhoto, "cover")){
                cropImageView.setAspectRatio(48,20);//CHANGE SHAPE OF CROP VIEW TO RECTANGULAR (48,20)
                cropImageView.setFixedAspectRatio(true);

            }
            cropImageView.setImageBitmap(bitmap);//SET CROP IMAGEVIEW BITMAP TO IMAGE CHOOSED
            cropImageView.setVisibility(cropImageView.VISIBLE);//SET CROP IMAGEVIEW VISIBILITY TO VISIBLE
            uploadProfilePhoto.setVisibility(uploadProfilePhoto.INVISIBLE);//SET UPLOAD PROFILE PHOTO BUTTON VISIBILITY TO INVISIBLE
            cropProfileButton.setVisibility(cropProfileButton.VISIBLE);//SET CROP BUTTON VISIBILITY TO VISIBLE

            cursor.close();
        }
    }

}
