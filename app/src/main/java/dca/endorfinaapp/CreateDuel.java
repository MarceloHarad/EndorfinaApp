package dca.endorfinaapp;

import android.*;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class CreateDuel extends AppCompatActivity implements View.OnClickListener {

    //ATRIBUTES USED BY THIS CLASS
    ImageButton uploadPhoto;//BUTTON TO UPLOAD A PHOTO
    ImageButton cropButton;//BUTTON TO CROP A PHOTO
    Button finishPhoto;//BUTTON TO FINISH CHANGING A PHOTO
    ImageView previewPhoto;//IMAGEVIEW TO PREVIW PIC(AND COVER)
    Utility utility;// UILITY OBJECT TO CALL UTILITY METHODS
    CropImageView cropImageView;// CROP IMAGE VIEW TO CROP IMAGE
    String typePhoto;// TYPE OF PHOTO (PROFILE PIC OR COVER), PASSED FROM PROFILE ACTIVITY
    private Bitmap cropped;// BITMAP OF IMAGE AFTER CROP
    final int SELECT_PHOTO = 12345;//AUXILIAR VALUE TO ENTER GALLERY
    EditText description;//BOX TO WRITE DESCRIPTION OF MEDIA

    private Profile profile;//PROFILE OBJECT PASSED FROM PROFILE ACTIVITY
    private Intent intent;//USED TO PASS FROM ONE ACTIVITY TO ANOTHER
    private String nomeIntent;//NAME OF PROFILE TO GET OBJECT FROM ACTIVITY
    private Bundle b;//BUNDLE USED TO GET PROFILE OBJECT FROM OTHER ACTIVITY
    private String path;//PATH OF IMAGE UPLOADED
    private Context context;
    private SearchView searchProfile;//LISTVIEW OF RESULTS OF SEARCH PROFILE
    private ListView listSearchProfile;//SEARCH VIEW TO SEARCH PROFILE
    private String nameProfileSelected;
    private String profilePathSelected;
    private FeedDuel feedDuel;

    //SQLITE DATABASE

    private FirebaseDatabase firebaseDatabase;//FIREBASE DATABASE

    private FirebaseStorage firebaseStorage;//FIREBASE STORAGE

    private StorageReference storageReference;//REFERENCE TO FIREBASE STORAGE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_duel);

        //GET PROFILE FROM PROFILE ACTIVITY
        intent = getIntent();
        nomeIntent = intent.getStringExtra("name");
        b = intent.getExtras();
        if (b != null) {
            profile = (Profile) getIntent().getSerializableExtra(nomeIntent);
        }

        //INITIALIZATE PREVIEW PROFILE PHOTO VIEW
        previewPhoto = (ImageView) findViewById(R.id.previewDuelPhoto);

        //INITIALIZATE UPLOAD PROFILE PIC BUTTON
        uploadPhoto = (ImageButton) findViewById(R.id.uploadDuelPhoto);
        uploadPhoto.setOnClickListener(CreateDuel.this);

        //INITIALIZATE CROP PIC BUTTON
        cropButton = (ImageButton) findViewById(R.id.cropDuelPhoto);
        cropButton.setOnClickListener(CreateDuel.this);

        //INITIALIZATE CROP IMAGE VIEW
        cropImageView = (CropImageView) findViewById(R.id.cropImageView);
        cropImageView.setVisibility(cropImageView.INVISIBLE);

        //INITIALIZATE BUTTON TO FINISH PHOTO
        finishPhoto = (Button) findViewById(R.id.buttonFinishDuelPic);
        finishPhoto.setOnClickListener(CreateDuel.this);

        //INITIALIZATE UTILITY OBJECT
        utility = new Utility();

        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = firebaseDatabase.getInstance();

        //INITIALIZATE FEED
        feedDuel = new FeedDuel();
        feedDuel.uploadListFeed();

        //INITIALIZATE FIREBASE STORAGE
        firebaseStorage = firebaseStorage.getInstance();

        //INITIALIZATE CONTEXT
        context = this;

        //INITIALIZATE DESCRIPTION BOX
        description = (EditText) findViewById(R.id.description);

        storageReference = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com");

        searchProfile = (SearchView) findViewById(R.id.search_profile);
        listSearchProfile = (ListView) findViewById(R.id.list_search_profile);
        /*
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            SearchProfileAdapter searchProfileAdapter = new SearchProfileAdapter(this);
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchProfileAdapter.doMySearch(query);
            listSearchProfile.setAdapter(searchProfileAdapter);
        }
        */
        final SearchProfileAdapter searchProfileAdapter = new SearchProfileAdapter(context,profile);
        searchProfile.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                query = query.toLowerCase();
                searchProfileAdapter.doMySearch(query);
                listSearchProfile.setAdapter(searchProfileAdapter);
                listSearchProfile.setVisibility(View.VISIBLE);
                previewPhoto.setVisibility(View.INVISIBLE);
                uploadPhoto.setVisibility(View.INVISIBLE);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.e("FeedsActivity","CHANGE");
                return true;
            }
        });
        searchProfile.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                listSearchProfile.setVisibility(View.INVISIBLE);
                previewPhoto.setVisibility(View.VISIBLE);
                uploadPhoto.setVisibility(View.VISIBLE);
                return true;
            }
        });
    }


    //METHODS WHEN YOU CLICK ON ANY BUTTON
    @Override
    public void onClick(View v) {

        //IF YOU CLICK ON UPLOAD PHOTO BUTTON
        if (v.getId() == R.id.uploadDuelPhoto) {

            //ASK PERMISSION TO ENTER GALLERY IF ANDROID VERSION IS MARSHMALLOW
            if (utility.shouldAskPermission()) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            }

            //ENTER GALLERY
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        }

        //IF BOTTOM PRESSED IS CROPPED BUTTON
        if (v.getId() == R.id.cropDuelPhoto) {

            cropped = cropImageView.getCroppedImage();//GET IMAGE FROM CROP VIEW
            previewPhoto.setImageBitmap(cropped);//SET PREVIEW IMAGE VIEW TO CROPPED IMAGE(PROFILE PIC)
            cropImageView.setVisibility(cropImageView.INVISIBLE);//SET VISIBILITY OF CROPVIEW TO INVISIBLE
            previewPhoto.setVisibility(previewPhoto.VISIBLE);//SET VISIBILITY OF PREVIEW PHOTO TO VISIBLE(PROFILE PIC)
            cropButton.setVisibility(cropButton.INVISIBLE);//SET VISIBILITY OF CROP BUTTON TO INVISIBLE
            uploadPhoto.setVisibility(uploadPhoto.VISIBLE);// SET VISIBILITY OF UPLOAD PHOTO TO VISIBLE
        }

        //IF BUTTON PRESSED IS TO SHARE MEDIA
        if (v.getId() == R.id.buttonFinishDuelPic) {

            //IF YOU`VE ALREADY UPLOADED A PHOTO
            if (previewPhoto.getDrawable() != null) {

                //IF YOU`VE ALREADY WRITTEN A DESCRIPTION
                if (!description.getText().toString().isEmpty()) {

                    //CREATE LOADING DIALOG WHILE PHOTO HAS BEEN UPLOADED
                    final ProgressDialog progress = new ProgressDialog(this);
                    progress.setTitle("Loading");
                    progress.setMessage("Wait while your photo is being uploaded...");
                    progress.show();

                    //storeImage(cropped);//STORE IMAGE ON INTERNAL STORAGE(AND GET PATH FROM IT)
                    final String pathMedia = "duels/" + profile.getUserName() + "/" + profile.getListDuels().size() + ".jpg";
                    StorageReference refToMedia = storageReference.child(pathMedia);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                    Bitmap croppedThumbNail = utility.scaleDown(cropped, 250, true);
                    cropped = utility.scaleDown(cropped, 1000, true);
                    cropped.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                    croppedThumbNail.compress(Bitmap.CompressFormat.JPEG, 60, baos2);

                    final byte[] data = baos.toByteArray();
                    final byte[] data2 = baos2.toByteArray();

                    UploadTask uploadTask = refToMedia.putBytes(data);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            //Calendar date = Calendar.getInstance();//GET EXACT TIME OF UPLOAD
                            Calendar c = Calendar.getInstance();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                            String strDate = sdf.format(c.getTime());
                            String[] elementsDate = strDate.split("\\s+");
                            String finalDate = elementsDate[0] + " at " + elementsDate[1];
                            finalDate = finalDate.replace("-", "/");


                            final Duel duel = new Duel();
                            duel.setDescription(description.getText().toString());
                            duel.setAccepted(false);
                            duel.setDate(finalDate);
                            duel.setImagePathCreator(pathMedia);
                            duel.setProfilePathCreator("profiles/" + profile.getUserName());
                            duel.setProfilePathChallenger(profilePathSelected);
                            duel.setProfilePicPathCreator(profile.getProfilePic_path());
                            duel.setCreatorFullName(profile.getFirstName() + " " + profile.getLastName());

                            profile.getListDuels().add(duel);//ADD MEDIA TO PROFILE LIST OF MEDIAS

                            DatabaseReference refFeeds = firebaseDatabase.getReference("feedDuel");
                            feedDuel.addDuel(duel.getProfilePathCreator() + "/listDuels/" + profile.getListDuels().indexOf(duel));
                            refFeeds.setValue(feedDuel);

                            DatabaseReference ref = firebaseDatabase.getReference("profiles/" + profile.getUserName());
                            ref.child("listDuels").setValue(profile.getListDuels());

                            DatabaseReference refProfileChallenger = firebaseDatabase.getReference("profiles/" + duel.getProfilePathChallenger());

                            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    // Get user value
                                    Profile profileChallenger = dataSnapshot.getValue(Profile.class);
                                    List listDuelsChallenger = profileChallenger.getListDuelsToAccept();
                                    listDuelsChallenger.add(duel);
                                    DatabaseReference refDuelsProfileChallenger = firebaseDatabase.getReference(duel.getProfilePathChallenger() + "/listDuelsToAccept");
                                    refDuelsProfileChallenger.setValue(listDuelsChallenger);

 

                                    //CLOSE LOADING DIALOG
                                    progress.dismiss();

                                    //CREATE ALERT DIALOG TO SHOW THAT PHOTO WAS SHARED WITH SUCCESS
                                    AlertDialog.Builder helpBuilder = new AlertDialog.Builder(context);
                                    helpBuilder.setTitle("Success");
                                    helpBuilder.setMessage("Your media was successfully published");
                                    helpBuilder.setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent i = new Intent(getApplicationContext(), GameActivity.class);
                                                    i.putExtra("name", profile.getUserName());
                                                    i.putExtra(profile.getUserName(), profile);
                                                    startActivity(i);
                                                }
                                            });
                                    AlertDialog helpDialog = helpBuilder.create();
                                    helpDialog.show();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                    });
                }

            }
        }
    }

    @Override
    //METHOD CALLED WHEN PHOTO WAS CHOOSED FROM GALLERY
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            Uri pickedImage = data.getData();//IMAGE URI
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));//IMAGE PATH

            //TRANSFROM IMAGEPATH TO BITMAP
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

            previewPhoto.setVisibility(previewPhoto.INVISIBLE);// SET VISIBILITY OF PREVIEW PHOTO TO INVISIBLE

            cropImageView.setAspectRatio(1,2);
            cropImageView.setFixedAspectRatio(true);
            cropImageView.setImageBitmap(bitmap);//SET CONTENT OF CROPVIEW TO BITMAP
            cropImageView.setVisibility(cropImageView.VISIBLE);//SET VISIBILITY OF CROPVIEW TO VISIBLE
            uploadPhoto.setVisibility(uploadPhoto.INVISIBLE);//SET VISIBILITY OF UPLOADPHOTO BUTTON TO INVISIBLE
            cropButton.setVisibility(cropButton.VISIBLE);//SET VISIBILITY OF CROP BUTTON TO VISIBLE

            cursor.close();
        }
    }

    public void hideListProfiles(){
        listSearchProfile.setVisibility(View.INVISIBLE);
        previewPhoto.setVisibility(View.VISIBLE);
        uploadPhoto.setVisibility(View.VISIBLE);
        searchProfile.setQuery(nameProfileSelected,false);
    }
    public void onBackPressed() {
        if(listSearchProfile.getVisibility() == View.VISIBLE){
            this.hideListProfiles();
        }
        else{
            super.onBackPressed();
        }
    }






    public String getNameProfileSelected() {
        return nameProfileSelected;
    }

    public void setNameProfileSelected(String nameProfileSelected) {
        this.nameProfileSelected = nameProfileSelected;
    }

    public String getProfilePathSelected() {
        return profilePathSelected;
    }

    public void setProfilePathSelected(String profilePathSelected) {
        this.profilePathSelected = profilePathSelected;
    }
}