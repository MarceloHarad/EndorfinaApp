package dca.endorfinaapp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


//ACITIVITY USED TO REGISTER A NEW PROFILE
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {


    //ATRIBUTES TO THIS CLASS
    private EditText firstName;//PERSON FIRST NAME
    private EditText lastName;//PERSON LAST NAME
    private EditText userName;//USERNAME USED TO LOGIN
    private EditText password;//PASSWORD USED TO LOGIN
    private EditText retypePassword;//RETYPE PASSWORD TO SEE IF IT IS CORRECT

    private RadioGroup gender;//GROUP OF RADIOBUTTONS TO CHOOSE YOUR GENDER
    private RadioButton male;//GENDER--MALE
    private RadioButton female;//GENDER--FEMALE

    private Button register;//REGISTER BUTTON WHEN FINISHED

    private FirebaseDatabase firebaseDatabase; //DATABASE USING FIREBASE
    private DatabaseReference ref; //REFERENCE TO FIREBASE DATABASE

    private MultiAutoCompleteTextView listSports;//LIST OF SPORTS
    private List<String> arraySports;//ARRAY OF SPORTS


    //CONSTRUCTOR
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = FirebaseDatabase.getInstance();
        ref = firebaseDatabase.getReference();


        //INITIALIZATE FIRST NAME BOX
        firstName = (EditText) findViewById(R.id.firstName);

        //INITIALIZATE LAST NAME BOX
        lastName = (EditText) findViewById(R.id.lastName);

        //INITIALIZATE USERNAME BOX
        userName = (EditText) findViewById(R.id.userName);

        //INITIALIZATE PASSWORD BOX
        password = (EditText) findViewById(R.id.registerPassword);

        //INITIALIZATE RETYPEPASSWORD BOX
        retypePassword = (EditText) findViewById(R.id.retypePassword);


        //INITIALIZATE RADIOGROUP AND RADIO BUTTONS
        gender = (RadioGroup) findViewById(R.id.gender);
        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);


        //INITIALIZATE REGISTER BUTTON
        register = (Button) findViewById(R.id.button_registed);
        register.setOnClickListener(RegisterActivity.this);

        //LIST SPORTS
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.new_sport_list));
        listSports = (MultiAutoCompleteTextView) findViewById(R.id.chooseSportProfile);
        listSports.setAdapter(adapter);
        listSports.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());


    }

    //METHOD TO CHECK IF EDIT TEXT IS EMPTY
    private boolean checkIfEmpty(EditText field){
        if(field.getText().toString().isEmpty()){
            field.setError("This field is empty");
            return true;
        }
        else{
            return false;
        }
    }

    //METHOD WHEN YOU CLICK REGISTER BUTTON
    @Override
    public void onClick(View v) {
        DatabaseReference ref = firebaseDatabase.getReference();

        arraySports = Arrays.asList(listSports.getText().toString().split(",\\s*"));
        Log.e("RegisterActivity",arraySports.toString());

        final boolean[] isCorrect = {true};

        ref = firebaseDatabase.getReference(userName.getText().toString());
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(Profile.class) != null){
                    isCorrect[0] = false;
                    Log.e("RegisterActivity","OK");
                    userName.setError("This user name is already taken");
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        //CHECK IF ANY EDIT TEXT IS EMPTY
        if (checkIfEmpty(firstName) | checkIfEmpty(lastName) | checkIfEmpty(userName) | checkIfEmpty(password) | checkIfEmpty(retypePassword) | checkIfEmpty(userName)) {
            isCorrect[0] = false;

        }

        for(String i:arraySports){
            if(!Arrays.asList(getResources().getStringArray(R.array.new_sport_list)).contains(i)){
                Log.e("RegisterActivity",i);
                isCorrect[0] = false;
                listSports.setError("You choose a non valid sport. Choose new valid sports");
            }
        }


        //IF THERE ARE NO ERRORS, RUN THIS
        if (isCorrect[0]) {

            //START LOADING DIALOG WHILE PROFILE HAS NOT BEEN DOWNLOADED FROM DATABASE
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setTitle("Loading");
            progress.setMessage("Wait while we check your profile...");
            progress.show();


            //CREATE NEW PROFILE
            final Profile profile = new Profile();

            //GET SOME INFORMATION BASED ON EDIT TEXTS VALUES
            profile.setFirstName(firstName.getText().toString());
            profile.setLastName(lastName.getText().toString());
            profile.setUserName(userName.getText().toString());
            profile.setPassword(password.getText().toString());
            profile.setListSports(arraySports);

            //CREATE LIST PROFILE OBJECT
            FullNameList fullNameList = new FullNameList();
            fullNameList.setProfilePath("profiles/" + profile.getUserName());
            fullNameList.setProfilePicPath(profile.getProfilePic_path());
            fullNameList.setFullName(profile.getFirstName() + " " + profile.getLastName());

            DatabaseReference refFullName = firebaseDatabase.getReference("listFullName/" + profile.getFirstName().toLowerCase() + " " + profile.getLastName().toLowerCase());
            refFullName.setValue(fullNameList);

            //ADD PROFILE TO FIREBASE DATABASE
            ref = firebaseDatabase.getReference("profiles/" + profile.getUserName());
            ref.setValue(profile);


            //CLOSE LOADING DIALOG
            progress.dismiss();

            //ALERT DIALOG TO SHOW THAT IT WORKED. WHEN CLICK OK, GOTO PROFILE ACTIVITY AND SEND PROFILE OBJECT
            AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
            helpBuilder.setTitle("Success");
            helpBuilder.setMessage("Your profile has been created");
            helpBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //LOGIN AFTER REGISTRATION
                    Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
                    i.putExtra("name", profile.getUserName());
                    i.putExtra(profile.getUserName(), profile);
                    startActivity(i);
                }
            });

            AlertDialog helpDialog = helpBuilder.create();
            helpDialog.show();
        }

        //IF THERE ARE ANY ERRORS, RUN THIS
        else {


            //CREATE ALERT DIALOG SHOWING THAT ARE ERRORS AND YOU NEED TO CORRECT BEFORE REGISTERING
            AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
            helpBuilder.setTitle("Error");
            helpBuilder.setMessage("Correct all errors on your registration and try again");
            helpBuilder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

            AlertDialog helpDialog = helpBuilder.create();
            helpDialog.show();
        }
    }

        @Override
        //CHANGE ACTION WHEN PRESSED BACK BUTTON. LOGIN ACTIVITY NEEDS TO BE RECONSTRUCTED TO MAKE ALL BOXES EMPTY.
        public void onBackPressed () {
            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(i);
        }


    }
