package dca.endorfinaapp;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by marcelo on 13/06/16.
 */
public class Duel implements Serializable {

    private String imagePathCreator;
    private String imagePathChallenger;
    private String profilePicPathCreator;
    private String profilePicPathChallenger;
    private String creatorFullName;
    private String challengerFullname;
    private String profilePathCreator;
    private String profilePathChallenger;
    private String Date;
    private ArrayList<String> listLikesCreator;
    private ArrayList<String> listLikesChallenger;
    private String description;
    private Boolean accepted;


    public String getImagePathCreator() {
        return imagePathCreator;
    }

    public void setImagePathCreator(String imagePathCreator) {
        this.imagePathCreator = imagePathCreator;
    }

    public String getImagePathChallenger() {
        return imagePathChallenger;
    }

    public void setImagePathChallenger(String imagePathChallenger) {
        this.imagePathChallenger = imagePathChallenger;
    }

    public String getProfilePathCreator() {
        return profilePathCreator;
    }

    public void setProfilePathCreator(String profilePathCreator) {
        this.profilePathCreator = profilePathCreator;
    }

    public String getProfilePathChallenger() {
        return profilePathChallenger;
    }

    public void setProfilePathChallenger(String profilePathChallenger) {
        this.profilePathChallenger = profilePathChallenger;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public ArrayList<String> getListLikesCreator() {
        return listLikesCreator;
    }

    public void setListLikesCreator(ArrayList<String> listLikesCreator) {
        this.listLikesCreator = listLikesCreator;
    }

    public ArrayList<String> getListLikesChallenger() {
        return listLikesChallenger;
    }

    public void setListLikesChallenger(ArrayList<String> listLikesChallenger) {
        this.listLikesChallenger = listLikesChallenger;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public String getProfilePicPathCreator() {
        return profilePicPathCreator;
    }

    public void setProfilePicPathCreator(String profilePicPathCreator) {
        this.profilePicPathCreator = profilePicPathCreator;
    }

    public String getProfilePicPathChallenger() {
        return profilePicPathChallenger;
    }

    public void setProfilePicPathChallenger(String profilePicPathChallenger) {
        this.profilePicPathChallenger = profilePicPathChallenger;
    }

    public String getCreatorFullName() {
        return creatorFullName;
    }

    public void setCreatorFullName(String creatorFullName) {
        this.creatorFullName = creatorFullName;
    }

    public String getChallengerFullname() {
        return challengerFullname;
    }

    public void setChallengerFullname(String challengerFullname) {
        this.challengerFullname = challengerFullname;
    }

    public Duel() {
        this.imagePathCreator = new String();
        this.imagePathChallenger = new String();
        this.profilePicPathCreator = new String();
        this.creatorFullName = new String();
        this.profilePicPathChallenger = new String();
        this.profilePathCreator = new String();
        this.challengerFullname = new String();
        this.profilePathChallenger = new String();
        this.listLikesCreator = new ArrayList<>();
        this.Date = new String();
        this.listLikesChallenger = new ArrayList<>();
        this.accepted = false;
        this.description = new String();
    }


    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Duel)) {
            return false;
        }

        Duel that = (Duel) other;

        // Custom equality check here.
        return this.imagePathCreator.equals(that.imagePathCreator)
                && this.imagePathChallenger.equals(that.imagePathChallenger)
                && this.profilePicPathCreator.equals(that.profilePicPathCreator)
                && this.profilePicPathChallenger.equals(that.profilePicPathChallenger)
                && this.description.equals(that.description)
                && this.creatorFullName.equals(that.creatorFullName)
                && this.challengerFullname.equals(that.challengerFullname)
                && this.profilePathCreator.equals(that.profilePathCreator)
                && this.profilePathChallenger.equals(that.profilePathChallenger)
                && this.Date.equals(that.Date)
                && this.listLikesCreator.equals(that.listLikesCreator)
                && this.listLikesChallenger.equals(that.listLikesChallenger)
                && this.description.equals(that.description)
                && this.accepted.equals(that.accepted);
    }

    @Override
    public int hashCode() {
        int hashCode = 1;

        hashCode = hashCode * 37 + this.imagePathCreator.hashCode();
        hashCode = hashCode * 37 + this.imagePathChallenger.hashCode();
        hashCode = hashCode * 37 + this.profilePicPathCreator.hashCode();
        hashCode = hashCode * 37 + this.profilePicPathChallenger.hashCode();
        hashCode = hashCode * 37 + this.description.hashCode();
        hashCode = hashCode * 37 + this.creatorFullName.hashCode();
        hashCode = hashCode * 37 + this.challengerFullname.hashCode();
        hashCode = hashCode * 37 + this.profilePathCreator.hashCode();
        hashCode = hashCode * 37 + this.profilePathChallenger.hashCode();
        hashCode = hashCode * 37 + this.Date.hashCode();
        hashCode = hashCode * 37 + this.listLikesCreator.hashCode();
        hashCode = hashCode * 37 + this.listLikesChallenger.hashCode();
        hashCode = hashCode * 37 + this.description.hashCode();
        hashCode = hashCode * 37 + this.accepted.hashCode();


        return hashCode;
    }
}
