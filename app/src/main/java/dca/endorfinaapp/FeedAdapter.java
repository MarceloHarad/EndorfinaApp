package dca.endorfinaapp;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by marcelo on 30/05/16.
 */
public class FeedAdapter extends BaseAdapter {
    //ATRIBUTES TO THIS CLASS
    private Object mItems;//LIST OF MEDIAS OF THE GRIDVIEW
    private ArrayList<Media> listPhotos;//LIST OF MEDIAS OF LISTVIEW
    private final LayoutInflater mInflater;//INFLATER OF GRIDVIEW
    private Context context;//CONTEXT USED
    private Utility utility;//UTILITY CLASS TO ACCESS SOME UTILITY METHODS
    private FirebaseDatabase firebaseDatabase; // FIREBASE DATABASE
    private FirebaseStorage firebaseStorage; // FIREBASE STORAGE
    private StorageReference storageReference; // REFERENCE TO FIREBASE STORAGE
    private DatabaseReference databaseReference;// REFERENCE TO FIREBASE DATABASE
    private Feed feed;//FEED CLASS
    private Profile profile;//USER PROFILE
    private Feed feedaux;//FEED AUX TO UPLOAD ATRIBUTES TO FIREBASE

    //CONSTRUCTOR
    public FeedAdapter(Context context, Feed feed,Profile profile) {

        mInflater = LayoutInflater.from(context);
        this.context = context;
        utility = new Utility();


        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = firebaseDatabase.getInstance();

        //INTIALIZATE FIREBASE STORAGE
        firebaseStorage = firebaseStorage.getInstance();

        //INITILIZATE REFERENCE TO STORAGE
        storageReference = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com/");

        //INITIALIZATE REFERENCE TO DATABASE
        databaseReference = firebaseDatabase.getReference();

        //INITIALIZATE LIST PHOTOS
        listPhotos = feed.getListMedias();

        //INITIALIZATE PROFILE
        this.profile = profile;

    }
    //SOME METHDOS THAT ARE USED BY BASEADAPTER -----
    @Override
    public int getCount() {
        return listPhotos.size();
    }

    @Override
    public Media getItem(int i) {

        return  listPhotos.get(listPhotos.size() - i - 1);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    public Feed getFeedaux() {
        return feedaux;
    }

    public void setFeedaux(Feed feedaux) {
        this.feedaux = feedaux;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View v = view;
        final ImageView picture;
        final ProgressBar loading;
        final CircleImageView profilePic;
        final TextView fullName;
        final TextView date;
        final RelativeLayout layoutPhoto;
        final ImageButton likesButton;
        final ImageButton likesRedButton;
        final TextView numberLikes;
        final TextView numberComments;
        final TextView numberShares;

        if (v == null) {
            v = mInflater.inflate(R.layout.feed_item_layout, null);
            v.setTag(R.id.fullscreenPreview, v.findViewById(R.id.fullscreenPreview));
            v.setTag(R.id.progressBarFullScreen,v.findViewById(R.id.progressBarFullScreen));
            v.setTag(R.id.user_image,v.findViewById(R.id.user_image));
            v.setTag(R.id.user_name, v.findViewById(R.id.user_name));
            v.setTag(R.id.date_time, v.findViewById(R.id.date_time));
            v.setTag(R.id.layoutFullscreenPhoto, v.findViewById(R.id.layoutFullscreenPhoto));
            v.setTag(R.id.likesButton, v.findViewById(R.id.likesButton));
            v.setTag(R.id.likesRedButton, v.findViewById(R.id.likesRedButton));
            v.setTag(R.id.number_likes, v.findViewById(R.id.number_likes));
            v.setTag(R.id.number_comments, v.findViewById(R.id.number_comments));
            v.setTag(R.id.number_shares, v.findViewById(R.id.number_shares));
        }


        layoutPhoto = (RelativeLayout) v.getTag(R.id.layoutFullscreenPhoto);
        final RelativeLayout mFrame = (RelativeLayout) v.getTag(R.id.layoutFullscreenPhoto);
        mFrame.post(new Runnable() {

            @Override
            public void run() {
                RelativeLayout.LayoutParams mParams;
                mParams = (RelativeLayout.LayoutParams) mFrame.getLayoutParams();
                mParams.height = mFrame.getWidth();
                mFrame.setLayoutParams(mParams);
                mFrame.postInvalidate();
            }
        });

        picture = (ImageView)v.getTag(R.id.fullscreenPreview);
        loading = (ProgressBar) v.getTag(R.id.progressBarFullScreen);
        profilePic = (CircleImageView)v.getTag(R.id.user_image);
        fullName = (TextView)v.getTag(R.id.user_name);
        date = (TextView)v.getTag(R.id.date_time);
        likesButton = (ImageButton)v.getTag(R.id.likesButton);
        likesRedButton = (ImageButton)v.getTag(R.id.likesRedButton);
        numberLikes = (TextView)v.getTag(R.id.number_likes);
        numberComments = (TextView)v.getTag(R.id.number_comments);
        numberShares = (TextView)v.getTag(R.id.number_shares);


        final Media item = getItem(i);// GET MEDIA FROM POSITION I OF MEDIA LIST
        final long ONE_MEGABYTE = 1024 * 1024;
        if (item.getListLikes() != null){
            numberLikes.setText(String.valueOf(item.getListLikes().size()));
        }
        else{
            numberLikes.setText("0");
        }
        if (item.getComments() != null){
            numberComments.setText(String.valueOf(item.getComments().size()));
        }
        else{
            numberComments.setText("0");
        }
        if (item.getListShares() != null){
            numberShares.setText(String.valueOf(item.getListShares().size()));
        }
        else{
            numberShares.setText("0");
        }

        if (item.getListLikes() != null && item.getListLikes().contains(profile.getUserName())){
            likesButton.setVisibility(View.INVISIBLE);
            likesRedButton.setVisibility(View.VISIBLE);
            likesRedButton.setClickable(true);
            likesButton.setClickable(false);
        }
        likesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.getListLikes().add(item.getDate());
                DatabaseReference ref = firebaseDatabase.getReference(item.getProfilePath());
                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile profile2 = dataSnapshot.getValue(Profile.class);
                        if (!profile2.getListPhotos().get(profile2.getListPhotos().indexOf(item)).getListLikes().isEmpty()){
                            ArrayList<String> likes = profile2.getListPhotos().get(profile2.getListPhotos().indexOf(item)).getListLikes();
                            likes.add(profile.getUserName());
                            String path_profile = item.getProfilePath() + "/listPhotos/" + profile2.getListPhotos().indexOf(item) + "/listLikes";
                            String path_feed = "feed/listMedias/" + feedaux.getListMedias().indexOf(item) + "/listLikes";
                            DatabaseReference ref2 = firebaseDatabase.getReference(path_profile);
                            ref2.setValue(likes);
                            ref2 = firebaseDatabase.getReference(path_feed);
                            ref2.setValue(likes);
                            DatabaseReference ref3 = firebaseDatabase.getReference("profiles/"+profile.getUserName()+"/listLikes");
                            ref3.setValue(profile.getListLikes());
                            item.getListLikes().add(profile.getUserName());
                            notifyDataSetChanged();
                        }
                        else{
                            ArrayList<String> likes = new ArrayList<String>();
                            likes.add(profile.getUserName());
                            String path_profile = item.getProfilePath() + "/listPhotos/" + profile2.getListPhotos().indexOf(item) + "/listLikes";
                            String path_feed = "feed/listMedias/" + feedaux.getListMedias().indexOf(item) + "/listLikes";
                            DatabaseReference ref2 = firebaseDatabase.getReference(path_profile);
                            ref2.setValue(likes);
                            ref2 = firebaseDatabase.getReference(path_feed);
                            ref2.setValue(likes);
                            item.getListLikes().add(profile.getUserName());
                            notifyDataSetChanged();
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                    }
                };
                ref.addListenerForSingleValueEvent(postListener);
                likesButton.setVisibility(View.INVISIBLE);
                likesButton.setClickable(false);
                likesRedButton.setVisibility(View.VISIBLE);
                likesRedButton.setClickable(true);

            }
        });
        likesRedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.getListLikes().remove(item.getDate());
                DatabaseReference ref = firebaseDatabase.getReference(item.getProfilePath());
                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile profile2 = dataSnapshot.getValue(Profile.class);
                        ArrayList<String> likes = profile2.getListPhotos().get(profile2.getListPhotos().indexOf(item)).getListLikes();
                        likes.remove(profile.getUserName());
                        String path_profile = item.getProfilePath() + "/listPhotos/" + profile2.getListPhotos().indexOf(item) + "/listLikes";
                        String path_feed = "feed/listMedias/" + feedaux.getListMedias().indexOf(item) + "/listLikes";
                        DatabaseReference ref2 = firebaseDatabase.getReference(path_profile);
                        ref2.setValue(likes);
                        ref2 = firebaseDatabase.getReference(path_feed);
                        ref2.setValue(likes);
                        DatabaseReference ref3 = firebaseDatabase.getReference("profiles/"+profile.getUserName()+"/listLikes");
                        ref3.setValue(profile.getListLikes());
                        item.getListLikes().remove(profile.getUserName());
                        notifyDataSetChanged();
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                    }
                };
                ref.addListenerForSingleValueEvent(postListener);
                likesRedButton.setVisibility(View.INVISIBLE);
                likesButton.setVisibility(View.VISIBLE);
                likesRedButton.setClickable(false);
                likesButton.setClickable(true);
            }
        });

        StorageReference photosRef = storageReference.child(item.getImagepath());
        if(!item.getProfilePicPath().isEmpty()){
            StorageReference thumbnailRef = storageReference.child(item.getProfilePicPath());
            thumbnailRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                    profilePic.setImageBitmap(bitmap);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        }
        else{
            Drawable profilePicDefault = context.getResources().getDrawable(R.drawable.defaultprofile);
            profilePic.setImageDrawable(profilePicDefault);
        }

        fullName.setText(item.getProfileFullName());
        fullName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = firebaseDatabase.getReference(item.getProfilePath());
                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile profile2 = dataSnapshot.getValue(Profile.class);
                        Intent i = new Intent(context.getApplicationContext(), ProfileVisitorActivity.class);
                        i.putExtra("name",profile2.getUserName());
                        i.putExtra(profile2.getUserName(),profile2);
                        i.putExtra("profileUser",profile);
                        context.startActivity(i);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                    }
                };
                ref.addListenerForSingleValueEvent(postListener);
            }
        });
        date.setText(item.getDate());
        photosRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                picture.setImageBitmap(bitmap);
                loading.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });

        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, FullScreenMediaActivity.class);
                i.putExtra("media",item);
                i.putExtra("profile",profile);
                i.putExtra("lastActivity","Feeds");
                context.startActivity(i);
            }
        });
        return v;
    }

    public Object getmItems() {
        return mItems;
    }

    public void setListPhotos(ArrayList<Media> listPhotos) {
        this.listPhotos = listPhotos;
    }

    public ArrayList<Media> getListPhotos(){
        return this.listPhotos;
    }

    //END OF THESE METHODS-----
}
