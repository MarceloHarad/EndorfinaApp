package dca.endorfinaapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dca.endorfinaapp.CreateDuel;
import dca.endorfinaapp.FeedsActivity;
import dca.endorfinaapp.Profile;
import dca.endorfinaapp.ProfileActivity;
import dca.endorfinaapp.R;
import dca.endorfinaapp.UploadMediaActivity;

public class ListMyDuels extends AppCompatActivity implements View.OnClickListener{

    private ImageButton feeds;
    private ImageButton sport;
    private ImageButton photo;
    private ImageButton profileBtn;
    private ImageButton challenges;
    private Intent intent;
    private String nomeIntent;
    private Bundle b;
    private Profile profile;
    private List<Duel> myDuels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_my_duels);

        //GET PROFILE FROM LAST ACTIVITY
        intent = getIntent();
        nomeIntent = intent.getStringExtra("name");
        b = intent.getExtras();
        if (b != null) {
            profile = (Profile) getIntent().getSerializableExtra(nomeIntent);
        }

        //INITIALIZATE FEEDS BUTTON
        feeds = (ImageButton) findViewById(R.id.feeds);
        feeds.setOnClickListener(ListMyDuels.this);

        //INITIALIZATE(WTF?) SPORT BUTTON
        sport = (ImageButton) findViewById(R.id.sports);
        sport.setOnClickListener(ListMyDuels.this);

        //INITIALIZATE(WTF?) CAMERA BUTTON
        photo = (ImageButton) findViewById(R.id.photo);
        photo.setOnClickListener(ListMyDuels.this);

        //INITIALIZATE(WTF?) PROFILE BUTTON
        profileBtn = (ImageButton) findViewById(R.id.profile);
        profileBtn.setOnClickListener(ListMyDuels.this);

        //INITIALIZATE(WTF?) CHALLENGES BUTTON
        challenges = (ImageButton) findViewById(R.id.challenges);
        challenges.setOnClickListener(ListMyDuels.this);

        ArrayList<String> duels = new ArrayList<>();
        if (duels.isEmpty()) {
            myDuels = profile.getListDuels();
        }
        ListDuelsAdapter lda = new ListDuelsAdapter(this,profile);
        ListView listView = (ListView) findViewById(R.id.listDuels);
        listView.setAdapter(lda);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.feeds) {
            Intent i = new Intent(getApplicationContext(), FeedsActivity.class);
            i.putExtra("name", profile.getUserName());
            i.putExtra(profile.getUserName(), profile);
            startActivity(i);
        }
        if (v.getId() == R.id.profile) {
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            i.putExtra("name", profile.getUserName());
            i.putExtra(profile.getUserName(), profile);
            startActivity(i);
        }
        if (v.getId() == R.id.photo) {
            Intent i = new Intent(getApplicationContext(), UploadMediaActivity.class);
            i.putExtra("name", profile.getUserName());
            i.putExtra(profile.getUserName(), profile);
            startActivity(i);
        }
        if (v.getId() == R.id.sports) {
            Intent i = new Intent(getApplicationContext(), CreateDuel.class);
            i.putExtra("name", profile.getUserName());
            i.putExtra(profile.getUserName(), profile);
            startActivity(i);
        }
        if (v.getId() == R.id.challenges) {

        }
    }
    }