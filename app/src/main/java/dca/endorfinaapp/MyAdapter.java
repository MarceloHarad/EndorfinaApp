package dca.endorfinaapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//CLASS USED TO ADAPT GRIDVIEW TO LIST OF IMAGEVIEWS
final class MyAdapter extends BaseAdapter {

    //ATRIBUTES TO THIS CLASS
    private final List<Media> mItems = new ArrayList<Media>();//LIST OF MEDIAS OF THE GRIDVIEW
    private final LayoutInflater mInflater;//INFLATER OF GRIDVIEW
    private Context context;//CONTEXT USED
    private Utility utility;//UTILITY CLASS TO ACCESS SOME UTILITY METHODS
    private FirebaseDatabase firebaseDatabase; // FIREBASE DATABASE
    private FirebaseStorage firebaseStorage; // FIREBASE STORAGE
    private StorageReference storageReference; // REFERENCE TO FIREBASE STORAGE
    private int sizeImage;
    private Profile profile;

    //CONSTRUCTOR
    public MyAdapter(Context context, int height, int width,Profile profile) {

        mInflater = LayoutInflater.from(context);
        this.context = context;
        utility = new Utility();

        sizeImage = (width/3);

        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = firebaseDatabase.getInstance();

        //INTIALIZATE FIREBASE STORAGE
        firebaseStorage = firebaseStorage.getInstance();

        //INITILIZATE REFERENCE TO STORAGE
        storageReference = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com/");

        this.profile = profile;//PROFILE PASSED

    }


    //SOME METHDOS THAT ARE USED BY BASEADAPTER -----
    @Override
    public int getCount() {

        return mItems.size();
    }

    @Override
    public Media getItem(int i) {

        return mItems.get(mItems.size() - i - 1);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        final ImageView picture;
        final ProgressBar loading;


        if (v == null) {
            v = mInflater.inflate(R.layout.grid_item_layout, viewGroup, false);
            v.setTag(R.id.image, v.findViewById(R.id.image));
            v.setTag(R.id.loading,v.findViewById(R.id.loading));
        }

        picture = (ImageView) v.getTag(R.id.image);
        picture.getLayoutParams().height = sizeImage;
        picture.getLayoutParams().width = sizeImage;

        loading = (ProgressBar) v.getTag(R.id.loading);
        final Media item = getItem(i);// GET MEDIA FROM POSITION I OF MEDIA LIST

        StorageReference photosRef = storageReference.child(item.getThumbnailPath());

        final long ONE_MEGABYTE = 1024 * 1024;
        photosRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                picture.setImageBitmap(bitmap);
                picture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, FullScreenMediaActivity.class);
                        i.putExtra("media",item);
                        i.putExtra("profile",profile);
                        i.putExtra("lastActivity",context.getClass().getSimpleName().toString());
                        context.startActivity(i);
                    }
                });
                loading.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
        return v;
    }
    //END OF THESE METHODS-----


    //ADD MEDIA TO LIST OF MEDIAS
    public void addItemtoList(Media photo){

        mItems.add(photo);
        notifyDataSetChanged();
    }

}
