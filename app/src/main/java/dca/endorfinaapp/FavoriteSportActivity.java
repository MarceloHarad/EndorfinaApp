package dca.endorfinaapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dca.endorfinaapp.Utility;

import static dca.endorfinaapp.Utility.createImageHash;

public class FavoriteSportActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<String> mySports;
    ImageButton lstBtn;
    private Intent intent;//USED TO PASS FROM ACTIVITIES
    private String nomeIntent;//GET USERNAME FROM PROFILE THAT WAS USED ON ANOTHER ACTIVITY
    private Bundle b;//BUNDLE TO RECEIVE AN OBJECT FROM OTHER ACTIVITY
    private Profile profile;//PROFILE OBJECT WITH ALL ATRIBUTES USED
    private int[] images = new int[]{
      R.drawable.artes_marciais,R.drawable.atletismo,R.drawable.automobilismo,
      R.drawable.badminton, R.drawable.ballet, R.drawable.baseball,
      R.drawable.basquete, R.drawable.bodyboard, R.drawable.boxe,
      R.drawable.caiaque, R.drawable.canoagem, R.drawable.capoeira,
      R.drawable.ciclismo, R.drawable.corrida, R.drawable.crossfit,
      R.drawable.danca, R.drawable.escalada, R.drawable.esgrima,
      R.drawable.esqui, R.drawable.futebol, R.drawable.futebol_americano,
      R.drawable.futevolei, R.drawable.ginastica_olimpica, R.drawable.golfe,
      R.drawable.halterofilismo, R.drawable.handebol, R.drawable.hipismo,
      R.drawable.hoquei, R.drawable.jiu_jitsu, R.drawable.judo,
      R.drawable.karate, R.drawable.kitesurf, R.drawable.levantamento_de_peso,
      R.drawable.luta_livre, R.drawable.mergulho, R.drawable.mma,
      R.drawable.motocross, R.drawable.muay_thai, R.drawable.musculacao,
      R.drawable.nado_sincronizado, R.drawable.natacao, R.drawable.paraquedismo,
      R.drawable.patinacao, R.drawable.pesca, R.drawable.pilates,
      R.drawable.polidance, R.drawable.polo, R.drawable.polo_aquatico,
      R.drawable.rafting, R.drawable.rugby, R.drawable.saltos_ornamentais,
      R.drawable.skate, R.drawable.slackline, R.drawable.snowboard,
      R.drawable.stand_up_paddle, R.drawable.surf, R.drawable.tenis,
      R.drawable.tenis_de_mesa, R.drawable.treinamento_funcional, R.drawable.triatlhon,
      R.drawable.vela, R.drawable.volei, R.drawable.wakeboard,
      R.drawable.windsurf, R.drawable.yoga
    };
    private ImageButton feeds;
    private ImageButton sport;
    private ImageButton photo;
    private ImageButton profileBtn;
    private ImageButton challenges;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_sport);

        //INITIALIZATE FEEDS BUTTON
        feeds = (ImageButton) findViewById(R.id.feeds);
        feeds.setOnClickListener(FavoriteSportActivity.this);

        //INITIALIZATE(WTF?) SPORT BUTTON
        sport = (ImageButton) findViewById(R.id.sports);
        sport.setOnClickListener(FavoriteSportActivity.this);

        //INITIALIZATE(WTF?) CAMERA BUTTON
        photo = (ImageButton) findViewById(R.id.photo);
        photo.setOnClickListener(FavoriteSportActivity.this);

        //INITIALIZATE(WTF?) PROFILE BUTTON
        profileBtn = (ImageButton) findViewById(R.id.profile);
        profileBtn.setOnClickListener(FavoriteSportActivity.this);

        //INITIALIZATE(WTF?) CHALLENGES BUTTON
        challenges = (ImageButton) findViewById(R.id.challenges);
        challenges.setOnClickListener(FavoriteSportActivity.this);



        HashMap<String,Integer> hash = createImageHash(getResources().getStringArray(R.array.new_sport_list),images);

        //GET PROFILE FROM LAST ACTIVITY
        intent = getIntent();
        nomeIntent = intent.getStringExtra("name");
        b = intent.getExtras();
        if (b != null) {
            profile = (Profile) getIntent().getSerializableExtra(nomeIntent);
        }

        mySports = new ArrayList<String>(profile.getListSports());

        ListView listView = (ListView) findViewById(R.id.favorite_sport_list);
        //mySports = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.test_sport_list)));
        //listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_gallery_item, android.R.id.text1, mySports));
        lstBtn = (ImageButton) findViewById(R.id.list_button);
        lstBtn.setOnClickListener(FavoriteSportActivity.this);

        MyAZAdapter<String> adapter = new MyAZAdapter<String>(
                getApplicationContext(), R.layout.favorite_sport_item,
                mySports);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), SportActivity.class);
                i.putExtra("name",profile.getUserName());
                i.putExtra(profile.getUserName(),profile);
                i.putExtra("sport", String.valueOf(parent.getItemAtPosition(position)));
                startActivity(i);
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.list_button) {
            Intent i = new Intent(getApplicationContext(), SportListActivity.class);
            i.putExtra("name", profile.getUserName());
            i.putExtra(profile.getUserName(), profile);
            startActivity(i);
        }
        if(v.getId() == R.id.feeds){
            Intent i = new Intent(getApplicationContext(), FeedsActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.profile){
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.photo){
            Intent i = new Intent(getApplicationContext(), UploadMediaActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }
        if(v.getId() == R.id.sports){

        }
        if(v.getId() == R.id.challenges){
            Intent i = new Intent(getApplicationContext(), GameActivity.class);
            i.putExtra("name",profile.getUserName());
            i.putExtra(profile.getUserName(),profile);
            startActivity(i);
        }

    }

    class MyAZAdapter<T> extends ArrayAdapter<T> implements SectionIndexer {
        ArrayList<String> myElements;
        HashMap<String, Integer> azIndexer;
        String[] sections;
        Context mContext;
        int id;

        public MyAZAdapter(Context context, int textViewResourceId, List<T> objects) {
            super(context, textViewResourceId, objects);
            myElements = (ArrayList<String>) objects;
            azIndexer = new HashMap<String, Integer>(); //stores the positions for the start of each letter
            mContext = context;
            id = textViewResourceId;

            int size = myElements.size();
            for (int i = size - 1; i >= 0; i--) {
                String element = myElements.get(i);
                //We store the first letter of the word, and its index.
                azIndexer.put(element.substring(0, 1), i);
            }

            Set<String> keys = azIndexer.keySet(); // set of letters

            Iterator<String> it = keys.iterator();
            ArrayList<String> keyList = new ArrayList<String>();

            while (it.hasNext()) {
                String key = it.next();
                keyList.add(key);
            }
            Collections.sort(keyList);//sort the keylist
            sections = new String[keyList.size()]; // simple conversion to array
            keyList.toArray(sections);
        }

        public int getPositionForSection(int section) {
            String letter = sections[section];
            return azIndexer.get(letter);
        }

        public int getSectionForPosition(int position) {
            Log.v("getSectionForPosition", "called");
            return 0;
        }

        public Object[] getSections() {
            return sections; // to string will be called to display the letter
        }

        @Override
        public View getView(int position, View v, ViewGroup parent)
        {
            HashMap<String,Integer> hash = createImageHash(getResources().getStringArray(R.array.new_sport_list),images);
            View mView = v ;
            if(mView == null){
                LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                mView = vi.inflate(id, null);
            }

            TextView text = (TextView) mView.findViewById(R.id.favorite_sport_show);
            ImageView image = (ImageView) mView.findViewById(R.id.background);
            if(myElements.get(position) != null)
            {
                if(hash.containsKey(myElements.get(position))){
                    image.setImageResource(hash.get(myElements.get(position)));
                }
                else{
                    image.setImageResource(R.drawable.surf);
                }
                text.setTextColor(Color.WHITE);
                text.setText(myElements.get(position));
            }

            return mView;
        }
    }
    @Override
    //CHANGE ACTION WHEN BACK BUTTON IS PRESSED. ISTEAD OF GOING TO LAST ACTIVITY, WITHDRAW APP
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
        i.putExtra("name",profile.getUserName());
        i.putExtra(profile.getUserName(),profile);
        startActivity(i);
    }
}
