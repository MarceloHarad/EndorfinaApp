package dca.endorfinaapp;

import java.io.Serializable;

/**
 * Created by marcelo on 25/05/16.
 */
public class Comment implements Serializable {

    private String content;
    private String profilePicPath;
    private String fullname;
    private String profilePath;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getProfile() {
        return profilePath;
    }

    public void setProfile(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getProfilePicPath() {
        return profilePicPath;
    }

    public void setProfilePicPath(String profilePicPath) {
        this.profilePicPath = profilePicPath;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    Comment(){

    }

}

