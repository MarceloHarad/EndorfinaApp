package dca.endorfinaapp;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

//CLASS CREATED TO MANAGE PROFILES
public class Profile implements Serializable {

    //ALL ATRIBUTES OF A PROFILE
    private int id;//ID NOT USED YET
    private String userName;//USERNAME USED TO LOGIN
    private String password;//PASSWORD USED TO LOGIN
    private String firstName;//FIRST NAME OF THE PERSON
    private String LastName;//LAST NAME OF THE PERSON

    private List<Media> listPhotos;//LIST OF MEDIAS THAT THE PERSON HAS
    private List<Duel> listDuels;//LIST OF DUELS
    private List<Duel> listDuelsToAccept;//LIST OF PENDENT DUELS
    private List<Duel> listAcceptedDuels;//LIST OF ACCEPTED DUELS
    private List<String> listVideos;//LIST OF VIDEOS---NOT USED!
    private String profilePic_path;//PATH TO PROFILE PICTURE IN LOCAL STORAGE
    private String cover_path;//PATH TO COVER PHOTO IN LOCAL STORAGE
    private List<String> listSports;//LIST SPORTS
    private List<String> listFans;//LIST OF FANS
    private List<String> listLikes;//LIST LIKES MEDIAS



    //GETTERS AND SETTER FROM THIS CLASS --------------
    public String getUserName() {

        return userName;
    }

    public void setUserName(String userName) {

        this.userName = userName;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public List<String> getListVideos() {

        return listVideos;
    }

    public void setListVideos(List<String> listVideos) {

        this.listVideos = listVideos;
    }

    public List<Media> getListPhotos() {

        return listPhotos;
    }

    public void setListPhotos(List<Media> listPhotos) {

        this.listPhotos = listPhotos;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {

        return LastName;
    }


    public void setLastName(String lastName) {

        LastName = lastName;
    }


    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getProfilePic_path() {
        return profilePic_path;
    }

    public void setProfilePic_path(String profilePic_path) {
        this.profilePic_path = profilePic_path;
    }

    public String getCover_path() {
        return cover_path;
    }

    public void setCover_path(String cover_path) {
        this.cover_path = cover_path;
    }
    //END OF GETTERS AND SETTERS --------------------

    public List<String> getListSports() {
        return listSports;
    }

    public void setListSports(List<String> listSports) {
        this.listSports = listSports;
    }

    public List<String> getListFans() {
        return listFans;
    }

    public void setListFans(List<String> listFans) {
        this.listFans = listFans;
    }

    public List<String> getListLikes() {
        return listLikes;
    }

    public void setListLikes(List<String> listLikes) {
        this.listLikes = listLikes;
    }

    public List<Duel> getListDuels() {
        return listDuels;
    }

    public void setListDuels(List<Duel> listDuels) {
        this.listDuels = listDuels;
    }

    public List<Duel> getListDuelsToAccept() {
        return listDuelsToAccept;
    }

    public void setListDuelsToAccept(List<Duel> listDuelsToAccept) {
        this.listDuelsToAccept = listDuelsToAccept;
    }

    public List<Duel> getListAcceptedDuels() {
        return listAcceptedDuels;
    }

    public void setListAcceptedDuels(List<Duel> listAcceptedDuels) {
        this.listAcceptedDuels = listAcceptedDuels;
    }


    //CONSTRUCTOR
    public Profile() {

        this.listPhotos = new ArrayList<Media>();//LIST OF PHOTOS START AS AN EMPTY ARRAYLIST
        this.profilePic_path = new String();//PATH OF PROFILE PICTURE START AS AN EMPTY STRING
        this.cover_path = new String();//PATH OF COVER PHOTO START AS AND EMPTY STRING
        this.listFans = new ArrayList<>();
        this.listLikes = new ArrayList<>();
        this.listDuels = new ArrayList<>();
        this.listAcceptedDuels = new ArrayList<>();
        this.listDuelsToAccept = new ArrayList<>();
    }



}
