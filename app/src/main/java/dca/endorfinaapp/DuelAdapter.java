package dca.endorfinaapp;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by marcelo on 30/05/16.
 */
public class DuelAdapter extends BaseAdapter {
    //ATRIBUTES TO THIS CLASS
    private Object mItems;//LIST OF MEDIAS OF THE GRIDVIEW
    private ArrayList<Duel> listDuel;//LIST OF MEDIAS OF LISTVIEW
    private final LayoutInflater mInflater;//INFLATER OF GRIDVIEW
    private Context context;//CONTEXT USED
    private Utility utility;//UTILITY CLASS TO ACCESS SOME UTILITY METHODS
    private FirebaseDatabase firebaseDatabase; // FIREBASE DATABASE
    private FirebaseStorage firebaseStorage; // FIREBASE STORAGE
    private StorageReference storageReference; // REFERENCE TO FIREBASE STORAGE
    private DatabaseReference databaseReference;// REFERENCE TO FIREBASE DATABASE
    private Profile profile;//USER PROFILE
    private int size;
    private Feed feedaux;//FEED AUX TO UPLOAD ATRIBUTES TO FIREBASE
    private ArrayList<String> listPathDuels;

    //CONSTRUCTOR
    public DuelAdapter(Context context,Profile profile, int size, ArrayList<String> listPathDuels) {

        mInflater = LayoutInflater.from(context);
        this.context = context;
        utility = new Utility();

        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = firebaseDatabase.getInstance();

        //INTIALIZATE FIREBASE STORAGE
        firebaseStorage = firebaseStorage.getInstance();

        //INITILIZATE REFERENCE TO STORAGE
        storageReference = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com/");

        //INITIALIZATE REFERENCE TO DATABASE
        databaseReference = firebaseDatabase.getReference();

        this.size = size;

        listDuel = new ArrayList<>();

        for (int i =0; i < size;i++){
            listDuel.add(null);
        }

        for (int i = 0; i < size; i++){
            String pathDuel = listPathDuels.get(i);
            DatabaseReference refDuel = firebaseDatabase.getReference(pathDuel);
            final int finalI = i;
            refDuel.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue(Duel.class).getAccepted()){
                        listDuel.set(finalI,dataSnapshot.getValue(Duel.class));
                        notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        //INITIALIZATE PROFILE
        this.profile = profile;


    }
    //SOME METHDOS THAT ARE USED BY BASEADAPTER -----
    @Override
    public int getCount() {
        return size;
    }

    @Override
    public Duel getItem(int i) {
        return listDuel.get(listDuel.size() - i -1);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    public Feed getFeedaux() {
        return feedaux;
    }

    public void setFeedaux(Feed feedaux) {
        this.feedaux = feedaux;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        View v = view;
        final ImageView picture;
        final ImageView picture2;
        final ProgressBar loading;
        final CircleImageView profilePic;
        final CircleImageView profilePic2;
        final TextView fullName;
        final TextView fullName2;
        final TextView date;
        final RelativeLayout layoutPhoto;
        final ImageButton likesButton;
        final ImageButton likesButton2;
        final ImageButton likesRedButton;
        final ImageButton likesRedButton2;
        final TextView numberLikes;
        final TextView numberLikes2;

        if (v == null) {
            v = mInflater.inflate(R.layout.duel_item_layout, null);
            v.setTag(R.id.fullscreenPreview, v.findViewById(R.id.fullscreenPreview));
            v.setTag(R.id.fullscreenPreview2, v.findViewById(R.id.fullscreenPreview2));
            v.setTag(R.id.progressBarFullScreen,v.findViewById(R.id.progressBarFullScreen));
            v.setTag(R.id.user_image,v.findViewById(R.id.user_image));
            v.setTag(R.id.user_rival_image,v.findViewById(R.id.user_rival_image));
            v.setTag(R.id.user_name, v.findViewById(R.id.user_name));
            v.setTag(R.id.user_rival_name, v.findViewById(R.id.user_rival_name));
            v.setTag(R.id.date_time, v.findViewById(R.id.date_time));
            v.setTag(R.id.layoutFullscreenPhoto, v.findViewById(R.id.layoutFullscreenPhoto));
            v.setTag(R.id.likesButton, v.findViewById(R.id.likesButton));
            v.setTag(R.id.likesRivalButton, v.findViewById(R.id.likesRivalButton));
            v.setTag(R.id.likesRedButton, v.findViewById(R.id.likesRedButton));
            v.setTag(R.id.number_likes, v.findViewById(R.id.number_likes));
            v.setTag(R.id.number_rival_likes, v.findViewById(R.id.number_rival_likes));
        }


        layoutPhoto = (RelativeLayout) v.getTag(R.id.layoutFullscreenPhoto);
        final RelativeLayout mFrame = (RelativeLayout) v.getTag(R.id.layoutFullscreenPhoto);
        mFrame.post(new Runnable() {

            @Override
            public void run() {
                RelativeLayout.LayoutParams mParams;
                mParams = (RelativeLayout.LayoutParams) mFrame.getLayoutParams();
                mParams.height = mFrame.getWidth();
                mFrame.setLayoutParams(mParams);
                mFrame.postInvalidate();
            }
        });

        picture = (ImageView)v.getTag(R.id.fullscreenPreview);
        picture2 = (ImageView)v.getTag(R.id.fullscreenPreview2);
        loading = (ProgressBar) v.getTag(R.id.progressBarFullScreen);
        profilePic = (CircleImageView)v.getTag(R.id.user_image);
        profilePic2 = (CircleImageView)v.getTag(R.id.user_rival_image);
        fullName = (TextView)v.getTag(R.id.user_name);
        fullName2 = (TextView)v.getTag(R.id.user_rival_name);
        date = (TextView)v.getTag(R.id.date_time);
        likesButton = (ImageButton)v.getTag(R.id.likesButton);
        likesButton2 = (ImageButton)v.getTag(R.id.likesRivalButton);
        likesRedButton = (ImageButton)v.getTag(R.id.likesRedButton);
        numberLikes = (TextView)v.getTag(R.id.number_likes);
        numberLikes2 = (TextView)v.getTag(R.id.number_rival_likes);


        final Duel item = getItem(i);// GET MEDIA FROM POSITION I OF MEDIA LIST
        if (item == null){
            return v;
        }
        final long ONE_MEGABYTE = 1024 * 1024;
        if (item.getListLikesCreator() != null){
            numberLikes.setText(String.valueOf(item.getListLikesCreator().size()));
        }
        else{
            numberLikes.setText("0");
        }
        if (item.getListLikesChallenger() != null){
            numberLikes2.setText(String.valueOf(item.getListLikesChallenger().size()));
        }
        else{
            numberLikes2.setText("0");
        }

        StorageReference photosRef = storageReference.child(item.getImagePathCreator());
        if(!item.getProfilePicPathCreator().isEmpty()){
            StorageReference thumbnailRef = storageReference.child(item.getProfilePicPathCreator());
            thumbnailRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                    profilePic.setImageBitmap(bitmap);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        }
        else{
            Drawable profilePicDefault = context.getResources().getDrawable(R.drawable.defaultprofile);
            profilePic.setImageDrawable(profilePicDefault);
        }

        StorageReference photosRef2 = storageReference.child(item.getImagePathChallenger());
        if(!item.getProfilePicPathChallenger().isEmpty()){
            StorageReference thumbnailRef = storageReference.child(item.getProfilePicPathCreator());
            thumbnailRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                    profilePic2.setImageBitmap(bitmap);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        }
        else{

            Drawable profilePicDefault = context.getResources().getDrawable(R.drawable.defaultprofile);
            profilePic2.setImageDrawable(profilePicDefault);
        }

        fullName.setText(item.getCreatorFullName());
        fullName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = firebaseDatabase.getReference(item.getProfilePathCreator());
                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile profile2 = dataSnapshot.getValue(Profile.class);
                        Intent i = new Intent(context.getApplicationContext(), ProfileVisitorActivity.class);
                        i.putExtra("name",profile2.getUserName());
                        i.putExtra(profile2.getUserName(),profile2);
                        i.putExtra("profileUser",profile);
                        context.startActivity(i);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                    }
                };
                ref.addListenerForSingleValueEvent(postListener);
            }
        });

        fullName2.setText(item.getChallengerFullname());
        fullName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = firebaseDatabase.getReference(item.getProfilePathChallenger());
                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile profile2 = dataSnapshot.getValue(Profile.class);
                        Intent i = new Intent(context.getApplicationContext(), ProfileVisitorActivity.class);
                        i.putExtra("name",profile2.getUserName());
                        i.putExtra(profile2.getUserName(),profile2);
                        i.putExtra("profileUser",profile);
                        context.startActivity(i);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                    }
                };
                ref.addListenerForSingleValueEvent(postListener);
            }
        });



        date.setText(item.getDate());




        photosRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                picture.setImageBitmap(bitmap);
                loading.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
        photosRef2.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                picture2.setImageBitmap(bitmap);
                loading.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
        return v;
    }

    public Object getmItems() {
        return mItems;
    }

    public void setListPhotos(ArrayList<Media> listPhotos) {
        this.listDuel = listDuel;
    }

    public ArrayList<Duel> getListPhotos(){
        return this.listDuel;
    }

    //END OF THESE METHODS-----
}
