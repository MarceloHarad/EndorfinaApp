package dca.endorfinaapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
//INSPIRED AT: http://stackoverflow.com/questions/6475410/listview-with-alphabets-on-the-right-like-the-iphone-is-it-possible
public class SportListActivity extends Activity {
    ListView myListView;
    List<String> elements;
    private Intent intent;//USED TO PASS FROM ACTIVITIES
    private String nomeIntent;//GET USERNAME FROM PROFILE THAT WAS USED ON ANOTHER ACTIVITY
    private TextView nome;//TEXTVIEW THAT SHOWS PROFILE NAME
    private Bundle b;//BUNDLE TO RECEIVE AN OBJECT FROM OTHER ACTIVITY
    private Profile profile;//PROFILE OBJECT WITH ALL ATRIBUTES USED


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sport_list);

        // elements
        elements = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.new_sport_list)));
        // listview
        myListView = (ListView) findViewById(R.id.sport_list);
        View header = getLayoutInflater().inflate(R.layout.favorite_sport_header, null);
        myListView.setFastScrollEnabled(true);
        myListView.addHeaderView(header);
        MyAZAdapter<String> adapter = new MyAZAdapter<String>(
                getApplicationContext(), R.layout.sport_item,
                elements);
        myListView.setAdapter(adapter);

        intent = getIntent();
        nomeIntent = intent.getStringExtra("name");
        b = intent.getExtras();
        if (b != null) {
            profile = (Profile) getIntent().getSerializableExtra(nomeIntent);
        }

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), SportActivity.class);
                i.putExtra("name",profile.getUserName());
                i.putExtra(profile.getUserName(),profile);
                i.putExtra("sport",String.valueOf(parent.getItemAtPosition(position)));
                startActivity(i);
            }
        });
    }

    class MyAZAdapter<T> extends ArrayAdapter<T> implements SectionIndexer {
        ArrayList<String> myElements;
        HashMap<String, Integer> azIndexer;
        String[] sections;
        Context mContext;
        int id;

        public MyAZAdapter(Context context, int textViewResourceId, List<T> objects) {
            super(context, textViewResourceId, objects);
            myElements = (ArrayList<String>) objects;
            azIndexer = new HashMap<String, Integer>(); //stores the positions for the start of each letter
            mContext = context;
            id = textViewResourceId;

            int size = elements.size();
            for (int i = size - 1; i >= 0; i--) {
                String element = elements.get(i);
                //We store the first letter of the word, and its index.
                azIndexer.put(element.substring(0, 1), i);
            }

            Set<String> keys = azIndexer.keySet(); // set of letters

            Iterator<String> it = keys.iterator();
            ArrayList<String> keyList = new ArrayList<String>();

            while (it.hasNext()) {
                String key = it.next();
                keyList.add(key);
            }
            Collections.sort(keyList);//sort the keylist
            sections = new String[keyList.size()]; // simple conversion to array
            keyList.toArray(sections);
        }

        public int getPositionForSection(int section) {
            String letter = sections[section];
            return azIndexer.get(letter);
        }

        public int getSectionForPosition(int position) {
            Log.v("getSectionForPosition", "called");
            return 0;
        }

        public Object[] getSections() {
            return sections; // to string will be called to display the letter
        }

        @Override
        public View getView(int position, View v, ViewGroup parent)
        {
            View mView = v ;
            if(mView == null){
                LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                mView = vi.inflate(id, null);
            }

            TextView text = (TextView) mView.findViewById(R.id.sport_show);

            if(myElements.get(position) != null)
            {
                text.setTextColor(Color.GRAY);
                text.setText(myElements.get(position));
            }

            return mView;
        }
        }
    }

