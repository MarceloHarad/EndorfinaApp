package dca.endorfinaapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import static dca.endorfinaapp.Utility.createImageHash;

public class SportActivity extends AppCompatActivity implements View.OnClickListener{
    private Intent intent;//USED TO PASS FROM ACTIVITIES
    private String nomeIntent;//GET USERNAME FROM PROFILE THAT WAS USED ON ANOTHER ACTIVITY
    private TextView nome;//TEXTVIEW THAT SHOWS PROFILE NAME
    private Bundle b;//BUNDLE TO RECEIVE AN OBJECT FROM OTHER ACTIVITY
    private Profile profile;//PROFILE OBJECT WITH ALL ATRIBUTES USED
    private String nomeSport;
    private FirebaseDatabase firebaseDatabase; //DATABASE USING FIREBASE
    private DatabaseReference ref; //REFERENCE TO FIREBASE DATABASE
    private int[] images = new int[]{
            R.drawable.artes_marciais,R.drawable.atletismo,R.drawable.automobilismo,
            R.drawable.badminton, R.drawable.ballet, R.drawable.baseball,
            R.drawable.basquete, R.drawable.bodyboard, R.drawable.boxe,
            R.drawable.caiaque, R.drawable.canoagem, R.drawable.capoeira,
            R.drawable.ciclismo, R.drawable.corrida, R.drawable.crossfit,
            R.drawable.danca, R.drawable.escalada, R.drawable.esgrima,
            R.drawable.esqui, R.drawable.futebol, R.drawable.futebol_americano,
            R.drawable.futevolei, R.drawable.ginastica_olimpica, R.drawable.golfe,
            R.drawable.halterofilismo, R.drawable.handebol, R.drawable.hipismo,
            R.drawable.hoquei, R.drawable.jiu_jitsu, R.drawable.judo,
            R.drawable.karate, R.drawable.kitesurf, R.drawable.levantamento_de_peso,
            R.drawable.luta_livre, R.drawable.mergulho, R.drawable.mma,
            R.drawable.motocross, R.drawable.muay_thai, R.drawable.musculacao,
            R.drawable.nado_sincronizado, R.drawable.natacao, R.drawable.paraquedismo,
            R.drawable.patinacao, R.drawable.pesca, R.drawable.pilates,
            R.drawable.polidance, R.drawable.polo, R.drawable.polo_aquatico,
            R.drawable.rafting, R.drawable.rugby, R.drawable.saltos_ornamentais,
            R.drawable.skate, R.drawable.slackline, R.drawable.snowboard,
            R.drawable.stand_up_paddle, R.drawable.surf, R.drawable.tenis,
            R.drawable.tenis_de_mesa, R.drawable.treinamento_funcional, R.drawable.triatlhon,
            R.drawable.vela, R.drawable.volei, R.drawable.wakeboard,
            R.drawable.windsurf, R.drawable.yoga
    };
    private ArrayList<String> listSports;
    private ArrayList<Media> sportMedia;
    private Feed feed;
    private DatabaseReference databaseReference;
    private MyAdapter gridAdapter;
    private GridView gridView;
    private AppBarLayout appBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sport);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        CollapsingToolbarLayout ctl = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        setSupportActionBar(toolbar);
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final Context context = this;
        final GridView grid = (GridView) findViewById(R.id.gridView);

        appBar = (AppBarLayout)findViewById(R.id.app_bar);
        final AppBarLayout mFrame = (AppBarLayout) findViewById(R.id.app_bar);

        intent = getIntent();
        nomeIntent = intent.getStringExtra("name");
        nomeSport = intent.getStringExtra("sport");
        b = intent.getExtras();
        if (b != null) {
            profile = (Profile) getIntent().getSerializableExtra(nomeIntent);
        }

        final DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int heightScreen = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        //INITILIZATE GRIDVIEW ADAPTER WITH GRIDVIEW
        gridAdapter = new MyAdapter(context,heightScreen,width,profile);
        NestedScrollView myGridView = (NestedScrollView) findViewById(R.id.myGridView);
        gridView = (GridView) myGridView.findViewById(R.id.gridView);
        gridView.setAdapter(gridAdapter);
        gridView.setRecyclerListener(new AbsListView.RecyclerListener() {
            @Override
            public void onMovedToScrapHeap(View view) {
            }
        });
        HashMap<String,Integer> hash = createImageHash(getResources().getStringArray(R.array.new_sport_list),images);


        listSports = new ArrayList<String>(profile.getListSports());
        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = FirebaseDatabase.getInstance();
        ref = firebaseDatabase.getReference("profiles/" + profile.getUserName());
        databaseReference = firebaseDatabase.getReference("feed");

        ctl.setTitle(nomeSport);

        if(hash.containsKey(nomeSport)){
            ctl.setBackgroundResource(hash.get(nomeSport));
        }
        else{
            ctl.setBackgroundResource(R.drawable.surf);
        }

        if(listSports.contains(nomeSport)){
            fab.setImageResource(R.drawable.star_on);
        }
        else{
            fab.setImageResource(R.drawable.star_off);
        }

        feed = new Feed();
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(Feed.class) != null) {
                    feed = dataSnapshot.getValue(Feed.class);
                    for (ListIterator<Media> iter = feed.getListMedias().listIterator(); iter.hasNext(); ) {
                        Media a = iter.next();
                        if (a.getSport().equals(nomeSport)) {
                            gridAdapter.addItemtoList(a);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listSports.contains(nomeSport)) {
                    listSports.remove(nomeSport);
                    profile.getListSports().remove(nomeSport);
                    ref.child("listSports").setValue(listSports);
                    fab.setImageResource(R.drawable.star_off);
                }
                else{
                    listSports.add(nomeSport);
                    profile.getListSports().add(nomeSport);
                    ref.child("listSports").setValue(listSports);
                    fab.setImageResource(R.drawable.star_on);
                }
            }
        });

    }

            @Override
    //CHANGE ACTION WHEN BACK BUTTON IS PRESSED. ISTEAD OF GOING TO LAST ACTIVITY, WITHDRAW APP
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), FavoriteSportActivity.class);
        i.putExtra("name",profile.getUserName());
        i.putExtra(profile.getUserName(),profile);
        startActivity(i);
    }

    @Override
    public void onClick(View v) {

    }
}
