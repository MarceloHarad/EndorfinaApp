package dca.endorfinaapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import de.hdodenhof.circleimageview.CircleImageView;

public class FullScreenMediaActivity extends AppCompatActivity {

    private ImageView fullscreenPreview;//IMAGEVIEW OF FULLSCREEN PREVIEW
    private Media media;//MEDIA IMPORTED FROM PROFILE GRID VIEW

    private Bundle b;//BUNDLE TO PASS MEDIA
    private Intent intent;//INTENT FROM LAST ACTIVITY
    private FirebaseStorage firebaseStorage;//FIREBASE STORAGE
    private FirebaseDatabase firebaseDatabase;//FIREBASE DATABASE
    private DatabaseReference databaseReference;//REFERENCE TO DATABASE
    private StorageReference storageReference;//STORAGE REFERENCE

    private ProgressBar progressBar;//PROGRESS BAR IMAGE
    private RelativeLayout layoutPhoto;//LAYOUT FULLSCREEN PHOTO

    private EditText typeComment;
    private CircleImageView profilePic;
    private TextView fullName;
    private TextView date;
    private ImageButton likesButton;
    private ImageButton likesRedButton;
    private TextView numberLikes;
    private TextView numberComments;
    private TextView numberShares;
    private Profile profile;
    private Profile profile_uploaded_photo;
    private Feed feed;
    private ListView comments;
    private CommentAdapter commentsAdapter;
    private TextView seeMoreComments;
    private RelativeLayout main;
    private RelativeLayout layoutComments;
    private Boolean showCommentary;
    private RelativeLayout.LayoutParams initParams;
    private String lastScreen;
    private View animationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_media);

        showCommentary = false;

        animationView = (View)findViewById(R.id.animationView);

        firebaseStorage = firebaseStorage.getInstance();

        firebaseDatabase = firebaseDatabase.getInstance();

        databaseReference = firebaseDatabase.getReference();

        storageReference = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com/");

        progressBar = (ProgressBar)findViewById(R.id.progressBarFullScreen);
        //GET MEDIA FROM LAST ACTIVITY
        intent = getIntent();
        b = intent.getExtras();
        if (b != null){
            media = (Media)getIntent().getSerializableExtra("media");
            profile = (Profile)getIntent().getSerializableExtra("profile");
            lastScreen = intent.getStringExtra("lastActivity");
            Log.e("FullScreenMediaActivity",lastScreen);
            Log.e("FullScreenMediaActivity",profile.getFirstName());

        }

        databaseReference = firebaseDatabase.getReference("feed");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(Feed.class) != null) {
                    feed = dataSnapshot.getValue(Feed.class);
                } else {
                    feed = new Feed();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        DatabaseReference ref = firebaseDatabase.getReference(media.getProfilePath());
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                profile_uploaded_photo = dataSnapshot.getValue(Profile.class);
                Log.e("FullScreenMediaActivity",String.valueOf(profile_uploaded_photo.getListPhotos().size()));
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        ref.addListenerForSingleValueEvent(postListener);

        typeComment = (EditText)findViewById(R.id.typeComment);
        typeComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEND){

                    Comment comment = new Comment();
                    comment.setContent(typeComment.getText().toString());
                    comment.setFullname(profile.getFirstName() + " " + profile.getLastName());
                    comment.setProfile("profiles/"+profile.getUserName());
                    comment.setProfilePicPath(profile.getProfilePic_path());
                    String path_profile = media.getProfilePath() + "/listPhotos/" + profile_uploaded_photo.getListPhotos().indexOf(media) + "/comments";
                    String path_feed = "feed/listMedias/" + feed.getListMedias().indexOf(media) + "/comments";

                    media.getComments().add(comment);
                    if(showCommentary){
                        commentsAdapter.setListComments(media.getComments());
                    }
                    else{
                        if(media.getComments() != null && media.getComments().size() >2 ){
                            ArrayList mediaSliced = new ArrayList(media.getComments().subList(media.getComments().size()-2,media.getComments().size()));
                            commentsAdapter.setListComments(mediaSliced);
                        }
                        else{
                            commentsAdapter.setListComments(media.getComments());
                        }
                    }
                    DatabaseReference ref2 = firebaseDatabase.getReference(path_profile);
                    ref2.setValue(media.getComments());
                    ref2 = firebaseDatabase.getReference(path_feed);
                    ref2.setValue(media.getComments());

                    DatabaseReference ref = firebaseDatabase.getReference(media.getProfilePath());
                    ValueEventListener postListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            profile_uploaded_photo = dataSnapshot.getValue(Profile.class);
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            profile_uploaded_photo = new Profile();
                        }
                    };

                    ref.addListenerForSingleValueEvent(postListener);
                    databaseReference = firebaseDatabase.getReference("feed");
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue(Feed.class) != null) {
                                feed = dataSnapshot.getValue(Feed.class);
                            } else {
                                feed = new Feed();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    Toast.makeText(getApplicationContext(),getResources().getText(R.string.toast_comment),Toast.LENGTH_SHORT).show();
                    typeComment.setText(null);

                }
                return false;
            }
        });
        profilePic = (CircleImageView)findViewById(R.id.user_image);
        fullName = (TextView)findViewById(R.id.user_name);
        date = (TextView)findViewById(R.id.date_time);
        likesButton = (ImageButton)findViewById(R.id.likesButton);
        likesRedButton = (ImageButton)findViewById(R.id.likesRedButton);
        numberLikes = (TextView)findViewById(R.id.number_likes);
        numberComments = (TextView)findViewById(R.id.number_comments);
        numberShares = (TextView)findViewById(R.id.number_shares);

        if (media.getListLikes() != null){
            numberLikes.setText(String.valueOf(media.getListLikes().size()));
        }
        else{
            numberLikes.setText("0");
        }
        if (media.getComments() != null){
            numberComments.setText(String.valueOf(media.getComments().size()));
        }
        else{
            numberComments.setText("0");
        }
        if (media.getListShares() != null){
            numberShares.setText(String.valueOf(media.getListShares().size()));
        }
        else{
            numberShares.setText("0");
        }

        if (media.getListLikes() != null && media.getListLikes().contains(profile.getUserName())){
            likesButton.setVisibility(View.INVISIBLE);
            likesRedButton.setVisibility(View.VISIBLE);
            likesRedButton.setClickable(true);
            likesButton.setClickable(false);
        }
        likesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.getListLikes().add(media.getDate());
                    if (!profile_uploaded_photo.getListPhotos().get(profile_uploaded_photo.getListPhotos().indexOf(media)).getListLikes().isEmpty()){
                        ArrayList<String> likes = profile_uploaded_photo.getListPhotos().get(profile_uploaded_photo.getListPhotos().indexOf(media)).getListLikes();
                        likes.add(profile.getUserName());
                        String path_profile = media.getProfilePath() + "/listPhotos/" + profile_uploaded_photo.getListPhotos().indexOf(media) + "/listLikes";
                        String path_feed = "feed/listMedias/" + feed.getListMedias().indexOf(media) + "/listLikes";
                        DatabaseReference ref2 = firebaseDatabase.getReference(path_profile);
                        ref2.setValue(likes);
                        ref2 = firebaseDatabase.getReference(path_feed);
                        ref2.setValue(likes);
                        DatabaseReference ref3 = firebaseDatabase.getReference("profiles/"+profile.getUserName()+"/listLikes");
                        ref3.setValue(profile.getListLikes());
                        media.getListLikes().add(profile.getUserName());
                        numberLikes.setText(String.valueOf(media.getListLikes().size()));
                        DatabaseReference ref = firebaseDatabase.getReference(media.getProfilePath());
                        ValueEventListener postListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                profile_uploaded_photo = dataSnapshot.getValue(Profile.class);
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                profile_uploaded_photo = new Profile();
                            }
                        };
                        ref.addListenerForSingleValueEvent(postListener);
                        databaseReference = firebaseDatabase.getReference("feed");
                        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getValue(Feed.class) != null) {
                                    feed = dataSnapshot.getValue(Feed.class);
                                } else {
                                    feed = new Feed();
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                    else{
                        ArrayList<String> likes = new ArrayList<String>();
                        likes.add(profile.getUserName());
                        String path_profile = media.getProfilePath() + "/listPhotos/" + profile_uploaded_photo.getListPhotos().indexOf(media) + "/listLikes";
                        String path_feed = "feed/listMedias/" + feed.getListMedias().indexOf(media) + "/listLikes";
                        DatabaseReference ref2 = firebaseDatabase.getReference(path_profile);
                        ref2.setValue(likes);
                        ref2 = firebaseDatabase.getReference(path_feed);
                        ref2.setValue(likes);
                        media.getListLikes().add(profile.getUserName());
                        numberLikes.setText(String.valueOf(media.getListLikes().size()));
                        DatabaseReference ref = firebaseDatabase.getReference(media.getProfilePath());
                        ValueEventListener postListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                profile_uploaded_photo = dataSnapshot.getValue(Profile.class);
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                profile_uploaded_photo = new Profile();
                            }
                        };
                        ref.addListenerForSingleValueEvent(postListener);
                        databaseReference = firebaseDatabase.getReference("feed");
                        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getValue(Feed.class) != null) {
                                    feed = dataSnapshot.getValue(Feed.class);
                                } else {
                                    feed = new Feed();
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                    likesButton.setVisibility(View.INVISIBLE);
                    likesButton.setClickable(false);
                    likesRedButton.setVisibility(View.VISIBLE);
                    likesRedButton.setClickable(true);
            }
        });
        likesRedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.getListLikes().remove(media.getDate());
                ArrayList<String> likes = profile_uploaded_photo.getListPhotos().get(profile_uploaded_photo.getListPhotos().indexOf(media)).getListLikes();
                likes.remove(profile.getUserName());
                String path_profile = media.getProfilePath() + "/listPhotos/" + profile_uploaded_photo.getListPhotos().indexOf(media) + "/listLikes";
                String path_feed = "feed/listMedias/" + feed.getListMedias().indexOf(media) + "/listLikes";
                DatabaseReference ref2 = firebaseDatabase.getReference(path_profile);
                ref2.setValue(likes);
                ref2 = firebaseDatabase.getReference(path_feed);
                ref2.setValue(likes);
                DatabaseReference ref3 = firebaseDatabase.getReference("profiles/"+profile.getUserName()+"/listLikes");
                ref3.setValue(profile.getListLikes());
                media.getListLikes().remove(profile.getUserName());
                numberLikes.setText(String.valueOf(media.getListLikes().size()));
                likesRedButton.setVisibility(View.INVISIBLE);
                likesButton.setVisibility(View.VISIBLE);
                likesRedButton.setClickable(false);
                likesButton.setClickable(true);
        }
        });

        layoutPhoto = (RelativeLayout)findViewById(R.id.layoutFullscreenPhoto);
        final RelativeLayout mFrame = (RelativeLayout) findViewById(R.id.layoutFullscreenPhoto);

        mFrame.post(new Runnable() {

            @Override
            public void run() {
                RelativeLayout.LayoutParams mParams;
                mParams = (RelativeLayout.LayoutParams) mFrame.getLayoutParams();
                mParams.height = mFrame.getWidth();
                mFrame.setLayoutParams(mParams);
                mFrame.postInvalidate();
            }
        });


        fullscreenPreview = (ImageView)findViewById(R.id.fullscreenPreview);

        StorageReference photosRef = storageReference.child(media.getImagepath());

        final long ONE_MEGABYTE = 1024 * 1024;
        photosRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                fullscreenPreview.setImageBitmap(bitmap);
                progressBar.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });

        if(!media.getProfilePicPath().isEmpty()){
            StorageReference thumbnailRef = storageReference.child(media.getProfilePicPath());
            thumbnailRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes , 0, bytes .length);
                    profilePic.setImageBitmap(bitmap);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        }
        else{
            Drawable profilePicDefault = getResources().getDrawable(R.drawable.defaultprofile);
            profilePic.setImageDrawable(profilePicDefault);
        }
        fullName.setText(media.getProfileFullName());
        fullName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = firebaseDatabase.getReference(media.getProfilePath());
                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile profile2 = dataSnapshot.getValue(Profile.class);
                        Intent i = new Intent(getApplicationContext(), ProfileVisitorActivity.class);
                        i.putExtra("name",profile2.getUserName());
                        i.putExtra(profile2.getUserName(),profile2);
                        i.putExtra("profileUser",profile);
                        startActivity(i);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                    }
                };
                ref.addListenerForSingleValueEvent(postListener);
            }
        });

        date.setText(media.getDate());
        comments = (ListView)findViewById(R.id.commentsListView);
        comments.setScrollContainer(false);
        commentsAdapter = new CommentAdapter(this,profile);
        if(media.getComments() != null && media.getComments().size() >2 ){
            ArrayList mediaSliced = new ArrayList(media.getComments().subList(media.getComments().size()-2,media.getComments().size()));
            commentsAdapter.setListComments(mediaSliced);
        }
        else{
            commentsAdapter.setListComments(media.getComments());
        }

        comments.setAdapter(commentsAdapter);
        comments.setRecyclerListener(new AbsListView.RecyclerListener() {
            @Override
            public void onMovedToScrapHeap(View view) {
            }
        });
        main = (RelativeLayout)findViewById(R.id.main) ;
        layoutComments = (RelativeLayout)findViewById(R.id.layoutCommentList);
        seeMoreComments = (TextView)findViewById(R.id.seeMoreComment);
        seeMoreComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!showCommentary){
                    RelativeLayout.LayoutParams params_main = (RelativeLayout.LayoutParams) main.getLayoutParams();
                    RelativeLayout.LayoutParams params_comments = (RelativeLayout.LayoutParams) layoutComments.getLayoutParams();
                    params_comments.addRule(RelativeLayout.BELOW,0);
                    layoutComments.setLayoutParams(params_comments);
                    seeMoreComments.setText(getResources().getText(R.string.see_less_comments));
                    showCommentary = true;
                    commentsAdapter.setListComments(media.getComments());
                    comments.setScrollContainer(true);
                }
                else if(showCommentary){
                    RelativeLayout.LayoutParams params_comments = (RelativeLayout.LayoutParams) layoutComments.getLayoutParams();
                    params_comments.addRule(RelativeLayout.BELOW,R.id.layoutOptionsPhoto);
                    layoutComments.setLayoutParams(params_comments);
                    seeMoreComments.setText(getResources().getText(R.string.see_more_comments));
                    if(media.getComments() != null && media.getComments().size() >2 ){
                        ArrayList mediaSliced = new ArrayList(media.getComments().subList(media.getComments().size()-2,media.getComments().size()));
                        commentsAdapter.setListComments(mediaSliced);
                    }
                    else{
                        commentsAdapter.setListComments(media.getComments());
                    }
                    showCommentary = false;
                }


            }
        });


    }
    @Override
    public void onBackPressed(){
        if(showCommentary){
            RelativeLayout.LayoutParams params_comments = (RelativeLayout.LayoutParams) layoutComments.getLayoutParams();
            params_comments.addRule(RelativeLayout.BELOW,R.id.layoutOptionsPhoto);
            layoutComments.setLayoutParams(params_comments);
            seeMoreComments.setText(getResources().getText(R.string.see_more_comments));
            if(media.getComments() != null && media.getComments().size() >2 ){
                ArrayList mediaSliced = new ArrayList(media.getComments().subList(media.getComments().size()-2,media.getComments().size()));
                commentsAdapter.setListComments(mediaSliced);
            }
            else{
                commentsAdapter.setListComments(media.getComments());
            }
            showCommentary = false;
        }
        else {

            if (lastScreen.equals("Feeds")) {
                Intent i = new Intent(getApplicationContext(), FeedsActivity.class);
                i.putExtra("name", profile.getUserName());
                i.putExtra(profile.getUserName(), profile);
                startActivity(i);
            }

            if (lastScreen.equals("SportActivity")) {
                Intent i = new Intent(getApplicationContext(), SportActivity.class);
                i.putExtra("name", profile.getUserName());
                i.putExtra(profile.getUserName(), profile);
                i.putExtra("sport", media.getSport());
                startActivity(i);
            }
            if (lastScreen.equals("ProfileVisitorActivity")) {
                Intent i = new Intent(getApplicationContext(), ProfileVisitorActivity.class);
                i.putExtra("name", profile_uploaded_photo.getUserName());
                i.putExtra(profile_uploaded_photo.getUserName(), profile_uploaded_photo);
                i.putExtra("profileUser", profile);
                startActivity(i);
            }
            if (lastScreen.equals("ProfileActivity")) {
                Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
                i.putExtra("name", profile_uploaded_photo.getUserName());
                i.putExtra(profile_uploaded_photo.getUserName(), profile_uploaded_photo);
                startActivity(i);
            }
        }

    }
}
