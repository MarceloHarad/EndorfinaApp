package dca.endorfinaapp;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by marcelo on 30/05/16.
 */
public class Feed {

    private  ArrayList<Media> listMedias;//LIST OF MEDIAS
    private  FirebaseDatabase firebaseDatabase;//FIREBASE DATABASE
    private  DatabaseReference databaseReference;//REFERENCE TO DATABASE

    public Feed() {
        listMedias = new ArrayList<>();
        firebaseDatabase = firebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("feed");
    }

    public ArrayList<Media> getListMedias() {
        return this.listMedias;
    }

    public void setListMedias(ArrayList<Media> listMedias) {
        this.listMedias = listMedias;
    }

    public void addMedia(Media media){
        this.listMedias.add(media);
    }

    public void uploadListFeed(){


        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(Feed.class) != null){
                    Feed feed = dataSnapshot.getValue(Feed.class);
                    listMedias = feed.getListMedias();
                }
                else{
                    listMedias = new ArrayList<Media>();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
