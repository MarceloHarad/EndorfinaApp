package dca.endorfinaapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by marcelo on 06/06/16.
 */
public class ListDuelsAdapter extends BaseAdapter {
    //ATRIBUTES TO THIS CLASS
    private List<Duel> listDuels;
    private final LayoutInflater mInflater;//INFLATER OF GRIDVIEW
    private Context context;//CONTEXT USED
    private Utility utility;//UTILITY CLASS TO ACCESS SOME UTILITY METHODS
    private FirebaseDatabase firebaseDatabase; // FIREBASE DATABASE
    private DatabaseReference databaseReference;//DATABASE REFERECE
    private FirebaseStorage firebaseStorage; // FIREBASE STORAGE
    private StorageReference storageReference; // REFERENCE TO FIREBASE STORAGE
    private Profile profile_user;//PROFILE OF THE USER

    //CONSTRUCTOR
    public ListDuelsAdapter(Context context, Profile profile) {

        mInflater = LayoutInflater.from(context);
        this.context = context;
        utility = new Utility();

        //INITIALIZATE FIREBASE DATABASE
        firebaseDatabase = firebaseDatabase.getInstance();

        //INITIALIZATE DATABASE REFERENCE
        databaseReference = firebaseDatabase.getReference();

        //INTIALIZATE FIREBASE STORAGE
        firebaseStorage = firebaseStorage.getInstance();

        //INITILIZATE REFERENCE TO STORAGE
        storageReference = firebaseStorage.getReferenceFromUrl("gs://project-2318138782929867660.appspot.com/");

        listDuels = profile.getListDuelsToAccept();

        profile_user = profile;


    }


    //SOME METHDOS THAT ARE USED BY BASEADAPTER -----
    @Override
    public int getCount() {

        return listDuels.size();
    }

    @Override
    public Duel getItem(int i) {

        return listDuels.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        final CircleImageView profilePic;
        final TextView fullName;
        final TextView description;


        if (v == null) {
            v = mInflater.inflate(R.layout.my_duel_item, viewGroup, false);
            v.setTag(R.id.user_image, v.findViewById(R.id.user_image));
            v.setTag(R.id.user_name, v.findViewById(R.id.user_name));
            v.setTag(R.id.description, v.findViewById(R.id.description));
        }

        profilePic = (CircleImageView) v.getTag(R.id.user_image);
        fullName = (TextView) v.getTag(R.id.user_name);
        description = (TextView) v.getTag(R.id.description);
        final Duel item = getItem(i);// GET MEDIA FROM POSITION I OF MEDIA LIST
        if (!item.getImagePathCreator().isEmpty()) {
            StorageReference photosRef = storageReference.child(item.getImagePathCreator());
            final long ONE_MEGABYTE = 1024 * 1024;
            photosRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    profilePic.setImageBitmap(bitmap);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                }
            });
        } else {
            Drawable drawableProfile = context.getResources().getDrawable(R.drawable.defaultprofile);
            profilePic.setImageDrawable(drawableProfile);
        }
        fullName.setText(item.getCreatorFullName());
        description.setText(item.getDescription());
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = firebaseDatabase.getReference(item.getProfilePathCreator());
                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile profile = dataSnapshot.getValue(Profile.class);
                        Intent i = new Intent(context.getApplicationContext(), AcceptDuelActivity.class);
                        i.putExtra("name", profile.getUserName());
                        i.putExtra(profile.getUserName(), profile_user);
                        i.putExtra("duel", item);
                        context.startActivity(i);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                    }
                };
                ref.addListenerForSingleValueEvent(postListener);
            }
        });
        return v;
    }
    //END OF THESE METHODS-----

    public List<Duel> getListFullNameList() {
        return this.listDuels;
    }

    public void setListDuels(List<Duel> listDuels) {
        this.listDuels = listDuels;
        notifyDataSetChanged();
    }
}
